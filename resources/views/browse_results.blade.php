@extends('layouts.app')

@section('content')


<div class="results">
    @if(!$random)
        <p>list of {{$nb_photos}} photos, starting index: {{$index_start}}, from a list of {{$nb_photos_total}}</p>
    @else
        <p>list of random {{$nb_photos}} photos, from {{$nb_photos_total}}</p>
    @endif
    
    @if($woeid !== "all")
        <p>from WOEID {{$woeid}}</p>
    @endif
</div>

<div class="top_spacer">
<table style="width: 100%;">
    <tr>
        <th>Photo Id</th>
        <th>Photo Title</th>
        <th>Number of tags</th>
    </tr>
    @foreach($photo_list as $p)
     <tr>
        <td><a href="photo/{{$p->id}}" target="_blank">{{$p->id}}</a></td>
        <td><a href="photo/{{$p->id}}" target="_blank">{{$p->titre}}</a></td>
        <td>{{$nb_tags[$p->id]}}</td>
     </tr>
    @endforeach
</table>

</div>
    
    


<div class="top_spacer" id="rand_results_button">
    
  <?php $nextIndex = $index_start + $nb_photos; ?>
    
@if($random)
  <form action="browse" method="POST" class="form-horizontal">
     {{ csrf_field() }}
        <input type="hidden" name="woeid" id="woeid" class="form-control" value="{{{$woeid}}}">
        <input type="hidden" name="random" id="random" class="form-control" value="{{{$random}}}">
        <input type="hidden" name="nb_photos" id="nb_photos" class="form-control" value="{{{$nb_photos}}}"> 
        
        {{-- not needed hin these case --}}
        <input type="hidden" name="nb_photos_total" id="nb_photos_total" class="form-control" value="0">
        <input type="hidden" name="index_start" id="index_start" class="form-control" value="0">
        
        <button type="submit" class="btn btn-default">
            <i class="fa fa-chevron-right  "></i> Generate {{$nb_photos}} random photos
        </button>
    </form>
</div>

@elseif ($nextIndex < $nb_photos_total)
                                 
  <form action="browse" method="POST" class="form-horizontal">
     {{ csrf_field() }}
        <input type="hidden" name="woeid" id="woeid" class="form-control" value="{{{$woeid}}}">
        <input type="hidden" name="random" id="random" class="form-control" value="{{{$random}}}">
        <input type="hidden" name="nb_photos_total" id="nb_photos_total" class="form-control" value="{{{$nb_photos_total}}}">
        
      
        <input type="hidden" name="index_start" id="index_start" class="form-control" value="{{{$nextIndex}}}">
        <input type="hidden" name="nb_photos" id="nb_photos" class="form-control" value="{{$nb_photos}}"> 
        
        <button type="submit" class="btn btn-default">
            <i class="fa fa-chevron-right  "></i> Next {{$nb_photos}} photos
        </button>
    </form>

@endif

</div>


@endsection