@extends('layouts.app')

@section('content')


<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <div> The identification of the tags with Babelnet and Babelfy is finished for woeid : {{{ $woeid }}} .</div>
    <div> It was done for {{$nb_photos_processed}} photos over {{$nb_photos_total}}, starting at photo #{{{ $indexStart }}} </div>
</div>


@if (!empty($rest_tags))
<div class="top_spacer" >
     <div>{{sizeof($rest_tags)}} tags, have not been identified.</div>
</div>
@endif


<div class="top_spacer" id="results_button">
    Choose the next bunch settings :

@if ($indexStart < $nb_photos_total)
                                 
  <form action="search_next_babelnet" method="POST" class="form-horizontal">
     {{ csrf_field() }}
        <input type="hidden" name="woeid" id="woeid" class="form-control" value="{{{$woeid}}}">
        <label for="indexStart" class="col-sm-3 control-label">Start Index </label>
        <input type="number" name="indexStart" id="indexStart" class="form-control" value="{{{$nextIndex}}}">
        <label for="nbPhotos" class="col-sm-3 control-label">Number of Photos </label>
        <input type="number" name="nbPhotos" id="nbPhotos" class="form-control" value="10"> 
        <button type="submit" class="btn btn-default">
            <i class="fa fa-chevron-right  "></i> Next photos
        </button>
    </form>

@endif

</div>


@endsection