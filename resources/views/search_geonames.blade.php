@extends('layouts.app')

@section('content')
  <div class="subtitle">Search Geonames</div>


<form action="search_geonames" method="POST" class="form-horizontal">
            {{ csrf_field() }}
          
           
            <div class="form-group">
               <div>
                    <label for="woeid" class="col-sm-3 control-label">Choose WOEID</label>
                    <select name="woeid" >
                        @foreach ($woeids as $wid)
                            <option value="{{{$wid ->woeid}}}">{{{$wid->woeid}}}</option>
                        @endforeach
                    </select>
                </div>
                
                
                <div class="group-form">
                    
                   
                    <!--div class="small_top_spacer col-sm-6">
                         <input type="checkbox" name="bbox" value="bbox"> Use Bounding Box? <br>
                         <label for="BBox" class="col-sm-3 control-label">Enter Bounding Box coordinates</label>
                        <div>
                            <label for="north" class="col-sm-3 control-label">North</label>
                            <input type="number" name="north" id="north" class="form-control" value="46.19">
                        </div>
                        <div>
                            <label for="south" class="col-sm-3 control-label">South</label>
                            <input type="number" name="south" id="south" class="form-control" value="46.17">
                        </div>
                        <div>
                            <label for="west" class="col-sm-3 control-label">West</label>
                            <input type="number" name="west" id="west" class="form-control" value="6.12">
                        </div>
                        <div>
                            <label for="east" class="col-sm-3 control-label">East</label>
                            <input type="number" name="east" id="east" class="form-control" value="6.15">
                        </div>
                    </div-->
                    
                    
                    
                    <div class="small_top_spacer col-sm-6">
                        <label for="BBox" class="col-sm-3 control-label">Enter hierarchy names for selected WOEID</label>
                    <div class="info"> <i class="fa fa-info" style="padding-right:3px; color: darkgray;"></i><a href="https://www.flickr.com/places/info/{{$wid->woeid}}" target="_blank" >See place info</a></div>
                        <div>
                            <label for="locality" class="col-sm-3 control-label">Locality</label>
                            <input type="text" name="locality" id="locality" class="form-control" value="Carouge">
                        </div>
                        <div>
                            <label for="county" class="col-sm-3 control-label">County</label>
                            <input type="text" name="county" id="county" class="form-control" value="Geneva">
                        </div>
                        <div>
                            <label for="region" class="col-sm-3 control-label">Region</label>
                            <input type="text" name="region" id="region" class="form-control" value="Canton of Geneva">
                        </div>
                        <div>
                            <label for="country" class="col-sm-3 control-label">Country</label>
                            <input type="text" name="country" id="country" class="form-control" value="Switzerland">
                        </div>
                    </div>
                    
                </div>
        
                 
                </div>

            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> Search Geonames
                    </button>
                </div>
            </div>
     </form>


<form action="search_geonames_hierarchy" method="POST" class="form-horizontal top_spacer">
            {{ csrf_field() }}
          
           
            <div class="form-group">
               <div>
                    <label for="woeid" class="col-sm-3 control-label">Choose WOEID</label>
                    <select name="woeid" >
                        @foreach ($woeids as $wid)
                            <option value="{{{$wid ->woeid}}}">{{{$wid->woeid}}}</option>
                        @endforeach
                    </select>
                </div> 
                
                <div>
                    <label for="woeid" class="col-sm-3 control-label">Choose hierarchy level</label>
                    <select name="hierarchy_level" >
                         <option value="locality">locality</option>
                         <option value="county">county</option>
                         <option value="region">region</option>
                         <option value="country">country</option>
                         <option value="world">world</option>
                    </select>
                </div> 
                <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> Search Geonames Hierarchy
                    </button>
                </div>
            </div>
     </form>
        
@endsection