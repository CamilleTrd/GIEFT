@extends('layouts.app')

@section('content')


<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <div>Result page # {{{ $page }}} for woeid : {{{ $woeid }}}</div>
    <div> has been saved in DB.</div>
</div>

<div class="top_spacer" id="results_button">
    
    @if ($page<$pages)
        <form action="next_result_page" method="POST" class="form-vertical">
         {{ csrf_field() }}
            <input type="hidden" name="woeid" id="woeid" class="form-control" value="{{{ $woeid}}}">
            <input type="hidden" name="page" id="page" class="form-control" value="{{{ $page }}}"> 
            <button type="submit" class="btn btn-default">
                <i class="fa fa-chevron-right  "></i> Next Result Page
            </button>
        </form>
    @endif

</div>

@endsection