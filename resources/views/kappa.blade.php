@extends('layouts.app')

@section('content')
<div class="subtitle">Calculate Cohen's Kappa for a given WOEID</div>


 <form action="kappa" method="POST" class="form-horizontal">
            {{ csrf_field() }}

           
            <div class="form-group">
                <label for="woeid" class="col-sm-3 control-label">Choose WOEID</label>
                <select name="woeid" >
                    @foreach ($woeids as $wid)
                        <option value="{{{$wid ->woeid}}}">{{{$wid->woeid}}}</option>
                    @endforeach
                </select>
                
                <label for="user1" class="col-sm-3 control-label">User1 name</label>
                <input type="text" name="user1" id="user1" class="form-control" value="camtrd">
                 <label for="user2" class="col-sm-3 control-label">User2 name</label>
                <input type="text" name="user2" id="user2" class="form-control">
                
            </div>

            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> calculate Kappa
                    </button>
                </div>
            </div>
     </form>
        
@endsection