@extends('layouts.app')

@section('content')
<div class="subtitle">Photo {{$photo_id}}</div>

<div>{{$photo->titre}}
<br/> 
    <a href="{{$photo->url}}" target="_blank">{{$photo->url}}</a>     
    <br/>
    <p>WOEID --> {{$woeid}}</p>
    <p>You need to check the tags that should be in the "Semantic enhancement of places" category. <br/>Uncheck those already in "Semantic enhancement of places" if they shouldn't be.</p>
</div>
 
<h3 class="small_top_spacer">Categories</h3> 

<form action="{{$photo_id}}/rest_validation" method="POST" class="form-horizontal">
         {{ csrf_field() }}
    
     <input type="hidden" name="photo_id" id="photo_id" class="form-control" value="{{{$photo_id}}}">

    <span class="categ_span">
       @if(isset($sorted_tags['weather']))
        <div class="categ">
            <h5>Weather</h5>
            <ul>
            @foreach($sorted_tags['weather'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(isset($sorted_tags['temporal']))
        <div class="categ">
            <h5>Temporal</h5>
            <ul>
            @foreach($sorted_tags['temporal'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
         @endif
    </span>

    <span class="categ_span">
         @if(isset($sorted_tags['event']))
        <div class="categ">
            <h5>Event</h5>
            <ul>
            @foreach($sorted_tags['event'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif

         @if(isset($sorted_tags['color']))
        <div class="categ">
            <h5>Color</h5>
            <ul>
            @foreach($sorted_tags['color'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif
    </span>

    <span class="categ_span">
        @if(isset($sorted_tags['actor']))
        <div class="categ">
            <h5>Actor</h5>
            <ul>
            @foreach($sorted_tags['actor'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(isset($sorted_tags['transport']))
        <div class="categ">
            <h5>Transport</h5>
            <ul>
            @foreach($sorted_tags['transport'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif
    </span>

    <span class="categ_span">
        @if(isset($sorted_tags['meta']))
        <div class="categ">
            <h5>Unidentified</h5>
            <ul>
            @foreach($sorted_tags['meta'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(isset($sorted_tags['photo']))
        <div class="categ">
            <h5>Photo</h5>
            <ul>
            @foreach($sorted_tags['photo'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
        @endif
    </span>

    <span class="categ_span">
        @if(isset($sorted_tags['geo class']))
        <div class="categ">
            <h5>Geo Feature Class</h5>
            <ul>
            @foreach($sorted_tags['geo class'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}  <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
          @endif
        @if(isset($sorted_tags['geo feature']))
        <div class="categ">
            <h5>Geo Instance</h5>
            <ul>
            @foreach($sorted_tags['geo feature'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}  <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}"></li>
            @endforeach
            </ul>
        </div>
          @endif

         @if(isset($sorted_tags['rest']))
        <div class="categ_rest">
            <h5>Semantic enhancement of places</h5>
            <ul>
            @foreach($sorted_tags['rest'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} <input type="checkbox" name="tag_isrest[]" value="{{{$tag->id}}}" checked="checked"></li>
            @endforeach
            </ul>
        </div>
          @endif
    </span>

     <button type="submit" class="btn btn-default">
        <i class="fa fa-check-square-o "></i> Send REST validation
    </button>
</form>
        
@endsection