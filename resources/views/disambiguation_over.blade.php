@extends('layouts.app')

@section('content')


<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <div> The disambiguation of tags with SemLib is finished.</div>
    <div> It was done for the following woeid {{{$woeid}}}, and the following tags : </div>
</div>

<div class="top_spacer" >
        @foreach($tags_disamb as $tag)
            <p>{{ $tag->id }} -> {{ $tag->label }} </p>
        @endforeach
</div>


@endsection