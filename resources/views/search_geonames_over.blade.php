@extends('layouts.app')

@section('content')


<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <p> The identification of tags with Geonames is finished.</p>
    <p> It was done for the following WOEID : {{{ $woeid}}}</p>
    @if(!empty($hierarchy_level))
    <p> And the following hierarchy level : {{$hierarchy_level}}</p>
    @else
    <p> searching for hierarchy match.</p>
    @endif
</div>

<div class="top_spacer" >
    
@if (!empty($rest_tags))
     <div>{{sizeof($rest_tags)}} tags, have not been identified.</div>
@endif
    
    <div class="categ">
        The following tags have been found in GeoNames : 
        <ul>
        @foreach($tags_ided as $tag)
            <li>{{$tag->id}} > {{$tag->label}}</li>
        @endforeach
        </ul>
    </div>
    
    <form action="search_geonames" method="POST" class="form-horizontal small_top_spacer">
        {{ csrf_field() }}
        
        <input type="hidden" name="woeid" value="{{$woeid}}">
        <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-trash "></i> Delete Search Geonames table for WOEID
                    </button>
                </div>
            </div>
    </form>

</div>


@endsection