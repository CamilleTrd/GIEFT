@extends('layouts.app')

@section('content')

<div class="subtitle">Photo {{$photoID}}</div>

<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <div> Your validation of REST's tgs has been saved</div>
    
    @if (!empty($tagIDs_array))
    <div> It was done for the following tags id : </div>
    @endif
</div>

@if (!empty($tagIDs_array))
<div class="top_spacer categ" >
    <ul style="padding-left: 0px;">
        @foreach($tagIDs_array as $tid)
        <li>{{ $tid }} </li>
        @endforeach
    </ul>
</div>
 @endif

@endsection