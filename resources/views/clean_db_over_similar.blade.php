@extends('layouts.app')

@section('content')


<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <p> Similars photos have been identified in the DB until index {{$idx}}, over {{$count}} photos.</p>
    
    @if($over == false)
    <form action="clean_db_similar" method="POST" class="form-horizontal top_spacer">
            {{ csrf_field() }}
         <input type="hidden" name="photos_toDelete" value="{{$photos_toDelete}}">
         <input type="hidden" name="idx" value="{{$idx}}">
        <p>Process next 20 authors</p>
         <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-wrench "></i> Clean DB similars
                    </button>
                </div>
            </div>
     </form>
    @else 
        <p>The selected photos have been deleted from the DB.</p>
    @endif
    
@endsection