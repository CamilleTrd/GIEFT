@extends('layouts.app')

@section('content')
<div class="subtitle">Photo {{$photo_id}}</div>

<div>{{$photo->titre}}
<br/> 
    <a href="{{$photo->url}}" target="_blank">{{$photo->url}}</a>     
    <br/>
    <p>WOEID --> {{$woeid}}</p>
    <p>You can compare the tags in "Semantic Enhancement of Places" from what the algorithm found (left) <br/> and what was selected by user (right) to be part of this section</p>
</div>
 
<div class="compare_result_left">
<h3 class="small_top_spacer">Algo</h3> 

    <span class="categ_span">
       @if(isset($sorted_tags['weather']))
        <div class="categ">
            <h5>Weather</h5>
            <ul>
            @foreach($sorted_tags['weather'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(isset($sorted_tags['temporal']))
        <div class="categ">
            <h5>Temporal</h5>
            <ul>
            @foreach($sorted_tags['temporal'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
         @endif
    </span>

    <span class="categ_span">
         @if(isset($sorted_tags['event']))
        <div class="categ">
            <h5>Event</h5>
            <ul>
            @foreach($sorted_tags['event'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
        @endif

         @if(isset($sorted_tags['color']))
        <div class="categ">
            <h5>Color</h5>
            <ul>
            @foreach($sorted_tags['color'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
        @endif
    </span>

    <span class="categ_span">
        @if(isset($sorted_tags['actor']))
        <div class="categ">
            <h5>Actor</h5>
            <ul>
            @foreach($sorted_tags['actor'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(isset($sorted_tags['transport']))
        <div class="categ">
            <h5>Transport</h5>
            <ul>
            @foreach($sorted_tags['transport'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
        @endif
    </span>

    <span class="categ_span">
        @if(isset($sorted_tags['meta']))
        <div class="categ">
            <h5>Unidentified</h5>
            <ul>
            @foreach($sorted_tags['meta'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
        @endif

        @if(isset($sorted_tags['photo']))
        <div class="categ">
            <h5>Photo</h5>
            <ul>
            @foreach($sorted_tags['photo'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} </li>
            @endforeach
            </ul>
        </div>
        @endif
    </span>

    <span class="categ_span">
        @if(isset($sorted_tags['geo class']))
        <div class="categ">
            <h5>Geo Feature Class</h5>
            <ul>
            @foreach($sorted_tags['geo class'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
          @endif
        @if(isset($sorted_tags['geo feature']))
        <div class="categ">
            <h5>Geo Instance</h5>
            <ul>
            @foreach($sorted_tags['geo feature'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
          @endif

         @if(isset($sorted_tags['rest']))
        <div class="categ_rest">
            <h5>Semantic enhancement of places</h5>
            <ul>
            @foreach($sorted_tags['rest'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} </li>
            @endforeach
            </ul>
        </div>
          @endif
    </span>
</div>
        
<div class="compare_result_right">
    <h3 class="small_top_spacer">Test Result</h3> 
        <ul>
            @foreach($rest_tags as $t)
                <li>tag #{{$t->id}} : voted {{$t->count_tag}} times</li>
            @endforeach
        </ul>

    <div class ="small_top_spacer" >
        <h3>Precision &amp; Recall</h3>
        Precision = {{$precision_recall['precision']}} and Recall = {{$precision_recall['recall']}}.
        <br/><br/>
         FN count : {{$precision_recall['FN']['count']}}, with tags : 
            @foreach($precision_recall['FN']['tags'] as $tid)
               {{$tid}} ,
            @endforeach
        <br/>
        FP count : {{$precision_recall['FP']['count']}}, with tags : 
            @foreach($precision_recall['FP']['tags'] as $tid)
                {{$tid}} ,
            @endforeach
            <br/>
        TP count : {{$precision_recall['TP']['count']}}, with tags : 
            @foreach($precision_recall['TP']['tags'] as $tid)
                {{$tid}} ,
            @endforeach
           <br/>
        Total algo count : {{$precision_recall['rest_algo']['count']}}, and Total user count : {{$precision_recall['rest_users']['count']}}.
        <br/>
    
    </div>
    <br/>
    
     <div class ="small_top_spacer final" >
        <h3>Geo Semantic</h3>
         
         <span class="categ_span">
        @if(isset($sorted_tags['geo class']))
        <div class="categ">
            <h5>Geo Feature Class</h5>
            <ul>
            @foreach($sorted_tags['geo class'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
          @endif
        @if(isset($sorted_tags['geo feature']))
        <div class="categ">
            <h5>Geo Instance</h5>
            <ul>
            @foreach($coverage_geo_instances as $tag)
                <li>{{$tag->id}} > {{$tag->label}}</li>
            @endforeach
            </ul>
        </div>
          @endif

         @if(isset($sorted_tags['rest']))
        <div class="categ_rest">
            <h5>Semantic Enhancement of Places</h5>
            <ul>
            @foreach($sorted_tags['rest'] as $tag)
                <li>{{$tag->id}} > {{$tag->label}} </li>
            @endforeach
            </ul>
        </div>
          @endif
    </span>
         
    </div>
    
    
</div>




@endsection