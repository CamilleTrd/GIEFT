@extends('layouts.app')

@section('content')
     @if (Auth::check())
        <div> Choose a task. </div>
     @else
        <div> Please login. </div>
    @endif
@endsection