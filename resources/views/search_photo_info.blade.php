@extends('layouts.app')

@section('content')
<div class="subtitle">Retrieve Flickr Photo Infos</div>


 <form action="search_photo_infos" method="POST" class="form-horizontal">
            {{ csrf_field() }}

           
            <div class="form-group">
                <label for="woeid" class="col-sm-3 control-label">Choose WOEID</label>
                <select name="woeid" >
                    @foreach ($woeids as $wid)
                        <option value="{{{$wid ->woeid}}}">{{{$wid->woeid}}}</option>
                    @endforeach
                </select>
            </div>

            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> Get Photos Infos
                    </button>
                </div>
            </div>
     </form>
        
@endsection