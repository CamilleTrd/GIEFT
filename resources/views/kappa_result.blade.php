@extends('layouts.app')

@section('content')



<div class="results">
     <i class="fa fa-floppy-o" style="font-size: 25px;"></i>
    <p> The following values have been found for Cohen's kappa for WOEID {{$woeid}}</p>
   
</div>

<div class="top_spacer" >

 <table>
    <TR>
        <TH> </TH>  
        <TH> </TH> 
        <TH > {{$user2}}</TH>
        <TH> </TH> 
    </TR> 
    <TR> 
        <TH> </TH> 
        <TH> </TH> 
        <TH> yes </TH> 
        <TH> no </TH> 
    </TR> 
    <TR> 
        <TH> {{$user1}} </TH> 
        <TH> yes </TH>
        <TD> {{$kappa_a}} </TD>
        <TD> {{$kappa_b}} </TD>
    </TR> 
    <TR>
        <TH> </TH> 
        <TH> no </TH>
        <TD> {{$kappa_c}} </TD>
        <TD> {{$kappa_d}}</TD>
    </TR> 
</table>

</div>


@endsection