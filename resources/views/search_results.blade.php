@extends('layouts.app')

@section('content')


<div class="results">
    <div>Results for woeid : {{{ $woeid }}}</div>
    <div>Result page # {{{ $page }}}</div>
    <div>Total result pages : {{{ $pages }}}</div>
    <div>{{{ $total_photos }}} photos in Flickr with this woeid</div>
</div>

<div class="top_spacer" id="results_button">
    <form action="save_result_page" method="POST" class="form-vertical">
         {{ csrf_field() }}
        <input type="hidden" name="woeid" id="woeid" class="form-control" value="{{{ $woeid }}}">
        <input type="hidden" name="page" id="page" class="form-control" value="{{{ $page }}}">
        <button type="submit" class="btn btn-default">
            <i class="fa fa-database "></i> Save Page Results
        </button>
    </form>
    
    
@if ($page<$pages)
    <form action="next_result_page" method="POST" class="form-vertical">
     {{ csrf_field() }}
        <input type="hidden" name="woeid" id="woeid" class="form-control" value="{{{ $woeid}}}">
        <input type="hidden" name="page" id="page" class="form-control" value="{{{ $page }}}"> 
        <button type="submit" class="btn btn-default">
            <i class="fa fa-chevron-right  "></i> Next Result Page
        </button>
    </form>
@endif

</div>

@endsection