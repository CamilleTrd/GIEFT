@extends('layouts.app')

@section('content')
<div class="subtitle">Clean Photo List</div>


 <form action="clean_db_duplicates" method="POST" class="form-horizontal">
            {{ csrf_field() }}
        <p>Delete duplicate photos. 
     </p>
         <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-wrench "></i> Clean DB dublicates
                    </button>
                </div>
            </div>
     </form>



 <form action="clean_db_similar" method="POST" class="form-horizontal top_spacer">
            {{ csrf_field() }}
        <input type="hidden" name="photos_toDelete" value="">
         <input type="hidden" name="index" value="0">
        <p>Delete photos with similar tag list from same author.</p>
         <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-wrench "></i> Clean DB similars
                    </button>
                </div>
            </div>
     </form>
        
@endsection