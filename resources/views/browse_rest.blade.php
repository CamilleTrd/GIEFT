@extends('layouts.app')

@section('content')
<div class="subtitle">Browse results of REST test</div>


 <form action="browse_rest" method="POST" class="form-horizontal">
            {{ csrf_field() }}

           
            <div class="form-group">
                <label for="woeid" class="col-sm-3 control-label">Choose WOEID</label>
                <select name="woeid" >
                        <option value="all">All photos</option>
                    @foreach ($woeids as $wid)
                        <option value="{{{$wid ->woeid}}}">{{{$wid->woeid}}}</option>
                    @endforeach
                </select>
                <div>
                    <label for="random" class="col-sm-3 control-label">20 random photos from the WOEID</label>
                    <input type="checkbox" name="random" value="random">
                </div>
                <input type="hidden" name="nb_photos" id="nb_photos" class="form-control" value="50"> 
                <input type="hidden" name="nb_photos_total" id="nb_photos_total" class="form-control" value="0">
                <input type="hidden" name="index_start" id="index_start" class="form-control" value="0">
        
            </div> 

            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> Get Photos list
                    </button>
                </div>
            </div>
     </form>
        
       <p> For all pictures : Precision = {{$precision_recall['all']['precision']}} &amp; Recall = {{$precision_recall['all']['recall']}}</p> 
    @foreach($woeids as $wid)
         <p> For woeid {{$wid->woeid}} pictures : Precision = {{$precision_recall[$wid->woeid]['precision']}} &amp; Recall = {{$precision_recall[$wid->woeid]['recall']}}</p>
    @endforeach


@endsection