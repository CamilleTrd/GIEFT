<!--resources/views/layouts/app.blade.php -->


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>GIEFT</title>
   
        <link href='https://fonts.googleapis.com/css?family=Lato:400,300,100' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <style>
            html, body {
                height: 100%;
                font-family: 'Lato';
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: top;
                width: 100%;
            }

            .content {
                text-align: center;
                display: inline-block;
                font-weight: 300;
                width: 80%;
            }

            .title {
                font-weight: 100;
                font-size: 50px;
            }
            
            .subtitle{
                font-weight: 100;
                font-size: 20px; 
                padding-bottom: 5%;
            }
            
            .spacer {
                width: 100%;
                height: 5%;
            }
            
            .top_spacer{
                padding-top: 5%;
            }
            
            .small_top_spacer{
                padding-top: 1.5%;
            }
            
            .group-form{
                display: inline;
                width: inherit;
            }
            
            form.form-vertical{
                display: inline;
                margin-top: 10%;
            }
            
            a.home{
                color: black;
                text-decoration: none;
            }
          
            .navbar{
                color: black;
                text-decoration: none;
                display:block;
                margin:10px;
                margin-top: 3%;
                font-family: 'Lato';
            }
            
           
            ul li{
                list-style-type: none;
                 display: list-item;
            }
            
            .navbar ul{
                padding: 10px;    
            }
            
            .navbar ul li{
                list-style-type: none;
                display: inline-block;
            }
            
            .navbar ul li a{
                color: darkgray;
                text-decoration: none;
                margin-right: 10px;
            }
            
            .navbar ul li a.active{
                color: black;
            }
           
            .info{
                font-style: italic; 
                font-size: 15px;
            }
            
            .categ{
                display:block;
                text-align: center;
            }
            
            .categ_rest{ 
                display:block;
                text-align: center;
                color: darkcyan;
            }
            
            .categ ul, .categ_rest ul{
                padding: 5px;
            }
            
           
           .categ ul li, .categ_rest ul li, .compare_result_right ul li{
                display: list-item;
            }
            
            .compare_result_left {
                width: 50%;
                display: inline-block;
                float: left;
            }
            .compare_result_right {
                width: 50%;
                display: inline-block;
            }
            
            .final {
                border: solid 1px grey;
            }
            
            table, th, td {
                border: 1px solid black;
            }
            
           /**.categ_span{
                display: inline;
            }*/
          /**  .fa:before{
                color:black;
            }**/
            
            /* This only works with JavaScript, if it's not present, don't show loader */
            .no-js #loader { display: none;  }
            .js #loader { display: block; position: absolute; left: 100px; top: 0; }
            .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(../../images/loader.gif) center no-repeat #fff;
            }
            
           
        </style>
   
        <!-- CSS And JavaScript -->
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

         <script>
             
            /* Wait for window load
            $(window).load(function() {
                $(".se-pre-con").fadeOut("slow");;
            });*/

         $(function () {
                var path = window.location.pathname;
                path = path.replace(/\/$/, "");
                path = decodeURIComponent(path);

                $(".nav ul li a").each(function () {
                    var href = $(this).attr('href');
                    if (path.substring(0, href.length) === href) {
                        $(this).closest('li').addClass('active');
                    }
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
        
            <a href="{{ URL::to('/')}}" class= "home" > <div class="title">GIEFT</div> </a>
            <div>camtrd</div>
            
            
             <nav class="navbar navbar-connect">
                <ul>
                   
                    @if (Auth::guest())
                        <li><a href="{{ URL::to('/login') }}"><i class="fa fa-btn fa-sign-in"> Login</a></i></li>
                        
                    @else
                    
                        <li><a href="{{ URL::to('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
                       @if (Auth::user()->admin)
                             <li><a href="{{ URL::to('/register') }}"><i class="fa fa-btn fa-user-plus"> Register a new user</a></i></li>
                        @endif
                    @endif
                 </ul>
             
                
                @if (Auth::check())
                 
                 <p>Welcome, {{Auth::user()->name}}!</p>
                 
            <nav class="navbar navbar-default">
                <ul>
                     @if (Auth::user()->admin)
                    <li><a href="{{ URL::to('/woeid')}}" alt="Search Woeid"> <i class="fa fa-flickr" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/photo_info')}}" alt="Search Photos Infos"> <i class="fa fa-picture-o" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/geonames')}}" alt="Search Geonames"> <i class="fa fa-globe" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/babelnet')}}" alt="Search BabelNet"> <i class="fa fa-bold" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/disambiguation')}}" alt="Tag disambiguation"> <i class="fa fa-tags" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/categorisation')}}" alt="Tag categorisation"> <i class="fa fa-cubes" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/clean_db')}}" alt="Clean DB"> <i class="fa fa-wrench" style="font-size:35px;"></i></a></li>
                    <li style="padding: 2%"></li>
                    @endif
                     <li><a href="{{ URL::to('/browse')}}" alt="Browse"> <i class="fa fa-database" style="font-size:35px;"></i></a></li>
                    <li><a href="{{ URL::to('/browse_rest')}}" alt="Browse"> <i class="fa fa-list-alt" style="font-size:35px;"></i></a></li>
                     <li><a href="{{ URL::to('/kappa')}}" alt="Cohen's Kappa"> <i class="fa fa-hand-scissors-o" style="font-size:35px;"></i></a></li>
                </ul>
            </nav>
            
            @endif
        <!--div class="spacer"></div-->
        <div class="content">
            <!--div class="se-pre-con"></div-->
            @yield('content')
        </div>
     </div>
    </body>
</html>