@extends('layouts.app')

@section('content')
<div class="subtitle">Search Flickr</div>

 <form action="search_flickr_woeid" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <!-- Task Name -->
            <div class="form-group">
                <label for="woeid" class="col-sm-3 control-label">Enter WOEID</label>

                <div class="col-sm-6">
                    <input type="number" name="woeid" id="woeid" class="form-control" value="782017">
                </div>
            </div>

            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" name="search_flickr" class="btn btn-default">
                        <i class="fa fa-search "></i> Search Flickr
                    </button>
                </div>
            </div>
     </form>
        
@endsection