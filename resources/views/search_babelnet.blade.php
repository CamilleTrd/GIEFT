@extends('layouts.app')

@section('content')
<div class="subtitle">Search BabelNet</div>


 <form action="search_babelnet" method="POST" class="form-horizontal">
            {{ csrf_field() }}

           
            <div class="form-group">
                <label for="woeid" class="col-sm-3 control-label">Choose WOEID</label>
                <select name="woeid" >
                    @foreach ($woeids as $wid)
                        <option value="{{{$wid ->woeid}}}">{{{$wid->woeid}}}</option>
                    @endforeach
                </select>
            </div>

    Gathers the tags from each photo with the chosen WOEID, and process the tags as a set. <br/>
     Paginate photos in DB by bunch of photos.
            <div class="small_top_spacer col-sm-6">
                <label for="indexStart" class="col-sm-3 control-label">Index of first photo to process</label>
                <input type="number" name="indexStart" id="indexStart" class="form-control" value="0"> 
                <label for="nbPhotos" class="col-sm-3 control-label">Number of photos to process</label>
                <input type="number" name="nbPhotos" id="nbPhotos" class="form-control" value="30"> 
            </div>
     
            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> Search BabelNet 
                    </button>
                </div>
            </div>
     </form>
        
@endsection