@extends('layouts.app')

@section('content')
<div class="subtitle">Tags categorisation</div>


 <form action="categorisation" method="POST" class="form-horizontal">
            {{ csrf_field() }}
    <div class="small_top_spacer col-sm-6">
         <label for="indexStart" class="col-sm-3 control-label">Nongeo array Index start </label>
          <input type="number" name="indexStart" id="indexStart" class="form-control" value="0"> 
         <label for="nb" class="col-sm-3 control-label">Nb of items to process </label>
          <input type="number" name="nb" id="nb" class="form-control" value="100"> 
     </div>
            <!-- Search Button -->
            <div class="small_top_spacer form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search "></i> Start 
                    </button>
                </div>
            </div>
     </form>
        
@endsection