# GIEFT
## Geographic Information extraction form Flickr Tags

##### *Camille Tardy, University of Geneva, 2016*



Prototype built with Laravel in the context of my PhD Thesis.
Modular system that implements different services : 

* [Geonames](http://www.geonames.org/export/ws-overview.html)
* [Wikidata](https://www.wikidata.org/)
* [BabelFy](http://babelfy.org)
* [SML tool](http://www.semantic-measures-library.org/sml/)



### Set Up 

Insert your username or API key for each service
* Geonames username in app/Geonames.php
* BableNet private key in app/BaelNet.php
* Flickr API key in app/Flickr.php

Run SML tool. The dictionnary for SML tool is held in the *Dictionnary* folder at the root but must be moved in a separate path or the settings must be updated.


### Database
![DB Schema](/DB.png)