<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Photo', function(Blueprint $table)
		{
			$table->biginteger('id')->unique('idPhoto_UNIQUE')->unsigned();
			$table->float('lat', 10, 6)->nullable();
			$table->float('long', 10, 6)->nullable();
			$table->string('url', 45)->nullable();
			$table->string('titre', 45)->nullable();
			$table->string('description', 45)->nullable();
			$table->integer('woeid');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Photo');
	}

}
