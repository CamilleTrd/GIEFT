<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Tag', function(Blueprint $table)
		{
			$table->foreign('geo_id', 'fk_Tags_Geonames1')->references('id')->on('Geo')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('nonGeo_id', 'fk_Tags_NonGeo1')->references('id')->on('NonGeo')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Tag', function(Blueprint $table)
		{
			$table->dropForeign('fk_Tags_Geonames1');
			$table->dropForeign('fk_Tags_NonGeo1');
		});
	}

}
