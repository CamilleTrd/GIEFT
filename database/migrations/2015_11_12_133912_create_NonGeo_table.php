<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNonGeoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('NonGeo', function(Blueprint $table)
		{
			$table->string('id', 45)->primary();
			$table->string('service_name', 45)->nullable();
            $table->string('label', 100)->nullable();
            $table->string('id_wikidata_label', 45)->nullable();
            $table->string('label', 255)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('NonGeo');
	}

}
