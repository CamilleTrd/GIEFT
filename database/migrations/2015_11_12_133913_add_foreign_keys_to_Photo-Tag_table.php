<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPhotoTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Photo-Tag', function(Blueprint $table)
		{
			$table->foreign('tag_id', 'fk_Photo-Tags_Tags1')->references('id')->on('Tag')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('photo_id', 'fk_Photo-Tags_Photo1')->references('id')->on('Photo')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Photo-Tag', function(Blueprint $table)
		{
			$table->dropForeign('fk_Photo-Tags_Tags1');
			$table->dropForeign('fk_Photo-Tags_Photo1');
		});
	}

}
