<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRESTvalidationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('RESTvalidation', function(Blueprint $table)
		{
			$table->increments('idREST_validation')->unique('idREST_validation_UNIQUE');
            $table->biginteger('PhotoTag_photo_id')->unsigned();
			$table->integer('PhotoTag_tag_id')->index('fk_REST_validation_PhotoTag1_idx');
			$table->boolean('isREST');
			$table->primary(['idREST_validation']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('PhotoTag');
	}

}
