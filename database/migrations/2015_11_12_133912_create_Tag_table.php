<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Tag', function(Blueprint $table)
		{
			$table->increments('id')->unique('idTags_UNIQUE');
			$table->boolean('isGeo')->nullable();
			$table->string('geo_id', 45)->nullable()->index('fk_Tags_Geonames1_idx');
			$table->string('nonGeo_id', 45)->nullable()->index('fk_Tags_NonGeo1_idx');
            $table->string('lang', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Tag');
	}

}
