<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGeoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Geo', function(Blueprint $table)
		{
			$table->string('id', 45)->primary();
			$table->boolean('is_class_code')->nullable();
			$table->string('service_name', 45)->nullable();
            $table->string('geo_label', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Geo');
	}

}
