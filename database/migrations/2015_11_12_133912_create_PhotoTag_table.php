<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotoTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('PhotoTag', function(Blueprint $table)
		{
			$table->biginteger('photo_id')->unsigned();
			$table->integer('tag_id')->index('fk_PhotoTags_Tags1_idx');
			$table->string('in_BoundingBox', 45)->nullable();
            $table->float('nongeo_weight', 8,4)->nullable();
			$table->primary(['photo_id','tag_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('PhotoTag');
	}

}
