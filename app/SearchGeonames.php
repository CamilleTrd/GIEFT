<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchGeonames extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'SearchGeonames';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;
    
   }
