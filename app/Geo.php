<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Geo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Geo';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;
    
    public function tag()
    {
        return $this->hasMany('Tag');
    }
}
