<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Photo';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;
    
    public function photo_tag()
    {
        return $this->hasMany('PhotoTag');
    }
}
