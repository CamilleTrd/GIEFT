<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Tag';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;

    public function photo_tag()
    {
        return $this->hasMany('PhotoTag');
    }
    
    public function geo()
    {
        return $this->belongsTo('Geo');
    }
    
    public function nongeo()
    {
        return $this->belongsTo('NonGeo', 'nonGeo_id');
    }
}
