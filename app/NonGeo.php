<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonGeo extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'NonGeo';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;
    
    
    public function tag()
    {
        return $this->hasMany('Tag');
    }
}
