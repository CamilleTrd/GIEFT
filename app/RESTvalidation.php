<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RESTvalidation extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'RESTvalidation';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;
    
   public function 	PhotoTag_photo_id()
    {
        return $this->belongsTo('PhotoTag');
    }
     
   
}
