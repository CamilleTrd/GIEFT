<?php

use App\Flickr;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\RESTvalidationController;
use App\Http\Controllers\Auth\AuthController;
    
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('home');
});
    
//home page
Route::get('/home', 'HomeController@index');



// Registration Routes...
Route::get('register', 'Auth\AuthController@showRegistrationForm');
Route::post('register', function(){
        $user['name'] = Input::get('name');
        $user['email'] = Input::get('email');
        $user['password'] = Input::get('password');
        
        $validator = AuthController::validator($user);
        if ($validator->fails()){
          // If validation fails redirect back to register.
          return Redirect::to('/register')
                ->withInput(Input::except('password'))
                ->withErrors($validator);
            print_r($validator);
        }else {
            AuthController::create($user);
        }
    
    });


// Authentication Routes...

Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', function(){
    // Getting all post data
    $data = Input::all();
    $validator = AuthController::validator_login($data);
    if ($validator->fails()){
      // If validation fails redirect back to register.
      return Redirect::to('/login')
            ->withInput(Input::except('password'))
            ->withErrors($validator);
    }else {
        AuthController::login($data);
    }
});

Route::get('auth/login', 'Auth\AuthController@showLoginForm');
Route::post('auth/login', function(){
    // Getting all post data
    $data = Input::all();
    $validator = AuthController::validator_login($data);
    if ($validator->fails()){
      // If validation fails redirect back to register.
      return Redirect::to('/login')
            ->withInput(Input::except('password'))
            ->withErrors($validator);
    }else {
        AuthController::login($data);
    }
});


Route::get('logout', 'Auth\AuthController@logout');


// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');
    
    
    

    Route::get('/woeid',  function () {
        return View::make('search_woeid');
    })->middleware(['auth']);

    Route::get('/photo_info', function () {
        //retrieve woeid list
        $woeids = PhotoController::get_woeids();
        return View::make('search_photo_info', ['woeids' => $woeids]);
    })->middleware(['auth']);

    Route::get('/geonames',  function () {
        //retrieve woeid list
        $woeids = PhotoController::get_woeids();
        return View::make('search_geonames', ['woeids' => $woeids]);
    })->middleware(['auth']);

    Route::get('/babelnet', function () {
        //retrieve woeid list
        $woeids = PhotoController::get_woeids();
        return View::make('search_babelnet', ['woeids' => $woeids]);
    })->middleware(['auth']);

    Route::get('/disambiguation', function () {
        //retrieve woeid list
        $woeids = PhotoController::get_woeids();
        return View::make('disambiguation', ['woeids' => $woeids]);
    })->middleware(['auth']);

    Route::get('/categorisation', function () {
        return View::make('categorisation');
    })->middleware(['auth']);

    Route::get('/browse', function () {
        $woeids = PhotoController::get_woeids();
        return View::make('browse', ['woeids' => $woeids]);
    })->middleware(['auth']);

    Route::get('/browse_rest', function () {
        $woeids = PhotoController::get_woeids();

        $precision_recall = RESTvalidationController::percision_recall_general("all");
         foreach($woeids as $k=>$w){
               $pr = RESTvalidationController::percision_recall_general($w->woeid);
               $precision_recall = $precision_recall + $pr;
         }
        
        return View::make('browse_rest', ['woeids' => $woeids, 'precision_recall' => $precision_recall]);
    })->middleware(['auth']);


    Route::get('/clean_db', function () {
        return View::make('clean_db');
    })->middleware(['auth']);

    Route::get('/kappa', function () {
             //retrieve woeid list
        $woeids = PhotoController::get_woeids();
        return View::make('kappa', ['woeids' => $woeids]);
    })->middleware(['auth']);


    Route::post('/search_photo_infos', 'PhotoController@get_photo_infos')->middleware(['auth']);

    //call search Flickr api
    Route::post('/search_flickr_woeid', function () {
        $woeid = Input::get('woeid');

        $flickr = new Flickr('37d26c8ade24fe0d55120b6043ed3e0c');

        // search flickr call api.
        $method = 'flickr.photos.search';
        $params = array(
            'woe_id' => $woeid,
            'per_page' => '500'
        );
       $search = $flickr->callMethod($method, $params);

       return View('search_results',
                         ['page' => $search['photos']['page'], 
                          'pages' => $search['photos']['pages'], 
                          'total_photos' => $search['photos']['total'], 
                          'woeid' => $woeid]);
    })->middleware(['auth']);

    //call Flickr for next result page
    Route::post('/next_result_page', function () {
        $woeid = Input::get('woeid');
        $page = Input::get('page');

        $flickr = new Flickr('37d26c8ade24fe0d55120b6043ed3e0c');

        // search flickr call api.
        $method = 'flickr.photos.search';
        $params = array(
            'woe_id' => $woeid,
            'per_page' => '500',
            'page' => $page+1
        );
       $search = $flickr->callMethod($method, $params);

       return View('search_results',
                         ['page' => $search['photos']['page'], 
                          'pages' => $search['photos']['pages'], 
                          'total_photos' => $search['photos']['total'], 
                          'woeid' => $woeid]);
    })->middleware(['auth']);

    // saving results page photos in DB
    Route::post('/save_result_page', 'PhotoController@save_result_page')->middleware(['auth']);

    //call geonames api
    Route::post('/search_geonames', 'TagController@search_geonames')->middleware(['auth']);
    //call geonames api
    Route::post('/search_geonames_hierarchy', 'TagController@search_geonames_hierarchy')->middleware(['auth']);


    //call babelnet api
    Route::post('/search_babelnet', 'TagController@search_babelnet')->middleware(['auth']);
    Route::post('/search_next_babelnet', 'TagController@search_babelnet')->middleware(['auth']);

    Route::post('/disambiguation', 'TagController@geo_nongeo_disambiguation')->middleware(['auth']);

    Route::post('/categorisation', 'NonGeoController@categorisation')->middleware(['auth']);
    
    Route::post('/browse', 'PhotoController@browse_photo')->middleware('auth');
    Route::post('/browse_rest', 'PhotoController@browse_photo_result')->middleware('auth');
    
    Route::any('/photo/{id}', function ($id) {
        $photo = PhotoController::find_by_id($id);
        $woeid = $photo->woeid;
        $all_tags = TagController::find_tags_by_photoid($id);
        $sorted_tags = TagController::sort_tags_by_categ($all_tags);

        return View::make('photo_view', ['photo_id'=> $id, 'photo' => $photo, 'sorted_tags' => $sorted_tags, 'woeid' => $woeid]);
    })->middleware('auth');

    Route::any('/photo/{id}/rest_validation', 'RESTvalidationController@rest_validation')->middleware('auth');

  Route::any('/photo_result/{id}', function ($photo_id) {
        $photo = PhotoController::find_by_id($photo_id);
        $woeid = $photo->woeid;
        $all_tags = TagController::find_tags_by_photoid($photo_id);
        $sorted_tags = TagController::sort_tags_by_categ($all_tags);
        $rest_result_tags = RESTvalidationController::result_tags($photo_id);
      
        $precision_recall = RESTvalidationController::percision_recall_photo($photo_id, $all_tags, $sorted_tags, $rest_result_tags, true);
      
      //if no geo features return empty array;
      if(array_key_exists('geo feature',$sorted_tags)){
            $geo_feat_array = $sorted_tags['geo feature'];
      } else{
            $geo_feat_array = array();
      }
      $geo_cov_instances = RESTvalidationController::filter_geo_instances($photo_id, $woeid,$geo_feat_array);
      
        return View::make('photo_view_result', ['photo_id'=> $photo_id, 'photo' => $photo, 'sorted_tags' => $sorted_tags, 'rest_tags' =>$rest_result_tags, 'precision_recall' => $precision_recall, 'woeid' => $woeid, 'coverage_geo_instances' => $geo_cov_instances ]);
    })->middleware('auth');

 Route::post('/clean_db_duplicates', 'PhotoController@cleanDB_duplicates')->middleware('auth');
 Route::post('/clean_db_similar', 'PhotoController@cleanDB_similar')->middleware('auth');
Route::post('/kappa', 'RESTvalidationController@kappa')->middleware(['auth']);


   