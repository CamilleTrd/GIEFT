<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
use Redirect;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
        $user = Auth::user();
         if ($user->admin !== true ) {
             //if not admin dont allow to go to desired page
            //return Redirect::to('/home');
             echo "Vous n'avez pas les droits nécessaires."
           //  Session::flash('status', 'You are not admin.');
        }
        
        return $next($request);
    }
}
