<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class DictionnaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    
    public function index()
    {
        //
    } */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    
    public function create()
    {
        //
    } */

    /**
     * Get the dictionnary item with the given ID.
     *
     * @return $dict 
     */
    public static function find_by_id($id) {
        $dict = DB::table('Dictionnary')->where('id', $id)->first();
        return $dict;
    }

	/**
     * Get the dictionnary item with the given lemma.
     * source = wordnet or wiktionary
     * @return $dict_array 
     */
    public static function find_by_label_source($lemma, $source) {
        $dict_array = DB::table('Dictionnary')
            ->where('lemma', $lemma)
            ->where('source', $source)
            ->get();
        return $dict_array;
    }


/**
     * Get the dictionnary item with the given lemma.
     *
     * @return $dict 
     */
    public static function find_by_label($lemma) {
        $dict = DB::table('Dictionnary')->where('lemma', $lemma)->get();
        return $dict;
    }
    
    
  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
 
    public function show($id)
    {
        //
    } */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function edit($id)
    {
        //
    }  */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function update(Request $request, $id)
    {
        //
    } */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
   
    public function destroy($id)
    {
        //
    } */
}
