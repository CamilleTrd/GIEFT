<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Tag;
use App\NonGeo;
use App\Wikidata;


class NonGeoController extends Controller
{
    /**
     * Get the non-geo with the given ID.
     *
     * @return $non-geo 
     */
    public static function find_by_id($id) {
        $nongeo = DB::table('NonGeo')->where('id', $id)->first();
        return $nongeo;
    }
    
    /**
     * Get all the non-geo items.
     * where wikidata not null
     * @return $nongeo_wiki_list 
     */
    public static function find_all_wikidata() {
        $nongeo_wiki_list = array();
        
        $nongeo_wiki_list = DB::table('NonGeo')->whereNotNull('id_wikidata_label')->get();
        return $nongeo_wiki_list;
    }
    
    
    /**
     * Get related tag from non geo id
     * @return $tag_list 
     */
    public static function find_related_tag($nongeo_id) {
        $tag_list = array();
        
        $tag_list = DB::table('Tag')->join('NonGeo', 'Tag.nonGeo_id', '=', 'NonGeo.id')->where('Tag.nonGeo_id', '=', $nongeo_id)->get();
        return $tag_list;
    }
    
    
    
    /**
    * set categ for nongeo item
    **/
    public static function set_categ_nongeo($nongeo, $categ) {
        DB::table('NonGeo')
            ->where('id', $nongeo->id)
            ->update(['category' => $categ]);
    }
    
    
    /**
     * Store a newly created resource in DB.
     */
    public static function save_NonGeo_inDB($nongeoID, $serviceName, $label, $categ, $wikidata_id)
    {
           // if $geo_id not already in DB, store geo element
            $nongeo_id = self::find_by_id($nongeoID);
        
            if ($nongeo_id === null){
                $nongeo = new NonGeo;
                $nongeo->id = $nongeoID;
                $nongeo->wiki_category = $categ;
                $nongeo->service_name = $serviceName;
                $nongeo->label = $label;
                if (isset($wikidata_id)){
                     $nongeo->id_wikidata_label = null;
                }else{
                    $nongeo->id_wikidata_label = $wikidata_id;
                }
               
                $nongeo->save(); 
            } 
    }

    
    /**
    * compare class id with ones setlected to reprst categ and return categ label
    **/
    public static function compare_category_class($class_id, $isP31){
        $categ = "";
        
        if($class_id == "11663" || $class_id == "1322005" ){
            // class weather or natural phenomenon
           $categ = "weather"; 
            
        }else if($class_id == "186081"){
            // class time interval
           $categ = "temporal"; 
            
        }else if($class_id == "1190554" || $class_id == "186408" ){
            // class event or point in time
           $categ = "event"; 
            
        }else if($class_id == "1075"){
            // class color
           $categ = "color"; 
            
        }else if($class_id == "830077" || $class_id == "7239" ){
            // class subkject or organism
           $categ = "actor"; 
            
        }else if($class_id == "11633"){
            // class photography
           $categ = "photo"; 
            
        }else if($class_id == "7590" || $class_id == "334166" ){
            // class transport or mode of transport
           $categ = "transport"; 
            
        }else if($class_id == "618123" ){
          // class geo object
            if($isP31){
                //is instance of
                $categ = "geo feature";
            }else{
                $categ = "geo class"; 
            }
        }else if($class_id == "15617994"){
            // class admin territorial
           $categ = "geo feature"; 
            
        }
        
        return $categ;
    }
    
    
    /**
    * retreive properties of wikidata item. from wikidata resp.
    echo " prop 279 - subclass of : ";
     echo " prop P361 - part of : ";
     echo " prop P31 - instance of : ";
     // foreach > ['mainsnak']['datavalue']['value']['numeric-id'] (w/out le Q)
     return array of prop result.
    **/
    public static function get_wikidata_prop($prop_id, $wikidata_rsp, $wiki_id){
        $property= 'P'.$prop_id;
        $p = array();
        
        $claims = $wikidata_rsp['entities'][$wiki_id]['claims'];

        if(array_key_exists($property, $claims)){
         /*  echo $wiki_id. " <pre>";
            print_r($claims[$property]);
            echo "</pre>";*/
         //   echo " prop ".$prop_id." --> ";

            foreach($claims[$property] as $key=>$c){
                if($c['mainsnak']['snaktype'] == 'value'){
                    $p_id = $c['mainsnak']['datavalue']['value']['numeric-id'];
                     $p[] = $p_id;
                 //   echo $p_id.", ";
                }else{
                    // no value for prop.
               //     echo " -- no item for property ".$property." <br/>";
                }
            }

        //    echo "<br/>";
        } else{
        //    echo " -- no item for property ".$property." <br/>";
        }
        
        return $p;
    }
    
    
    /**
    *  Recursive function to find categ with define depth level
    *
    **/
     public static function rec_search_wikidata_categ($wikidata, $wiki_id, $depth, $nongeo,  $isP31){
         
        if($depth >0){
            
          //  echo "<br/><br/> We are at depth : ".$depth.", with wiki id  = ".$wiki_id.", and nongeo : ".$nongeo->id."<br/> ";
            
            $depth--;
          
            $p31 = array();
            $p279 = array();
            $p361 = array();

            $categ_31 = "";
            $categ_279 = "";
            $categ_361 = "";
            $isfound=false;

            
            $id_wiki = strstr($wiki_id, 'Q', false);
            $categ_wiki_id = self::compare_category_class($id_wiki, $isP31);
            
            if(!empty($categ_wiki_id)){
                // if wiki_item is categ
                self::set_categ_nongeo($nongeo, $categ_wiki_id);
                $isfound=true;
            
            }else{
                // if categ not found.
                $wikidata_rsp = $wikidata->callMethod($wiki_id);  

                $p31 = self::get_wikidata_prop(31, $wikidata_rsp, $wiki_id);
                $p279 = self::get_wikidata_prop(279, $wikidata_rsp, $wiki_id);
                $p361 = self::get_wikidata_prop(361, $wikidata_rsp, $wiki_id);

                if(!empty($p31)){
                    foreach($p31 as $pid){
                        $isP31 = true;
                        $categ_31 = self::compare_category_class($pid, $isP31);
                        if(!empty($categ_31)){
                             // if categ found
                          //  echo " ## id :".$pid.", cat : ".$categ_31."<br/>";
                            self::set_categ_nongeo($nongeo, $categ_31);
                            $isfound=true;
                            break;
                        }else{
                            //categ not found jump up
                              //if none is categ then search n+1
                            $w_id = 'Q'.$pid;
                          //  echo " ** no categ found for id ".$pid.", calling self with wid : ".$w_id."<br/>";
                            self::rec_search_wikidata_categ($wikidata, $w_id, $depth, $nongeo,  $isP31);
                        }
                    }

                }
                if(!empty($p279) && !$isfound){
                    foreach($p279 as $pid){
                        $categ_279 = self::compare_category_class($pid, $isP31);
                        if(!empty($categ_279)){
                             // if categ found
                         //   echo " ## id :".$pid.", cat : ".$categ_279."<br/>";
                            self::set_categ_nongeo($nongeo, $categ_279);
                             $isfound=true;
                            break;
                        }else{
                            //if none is categ then search n+1
                            $w_id = 'Q'.$pid;
                         //   echo " ** no categ found for id ".$pid.", calling self with wid : ".$w_id."<br/>";
                            self::rec_search_wikidata_categ($wikidata, $w_id, $depth, $nongeo, $isP31);
                        }
                    }
                }
                if(!empty($p361) && !$isfound){
                    foreach($p361 as $pid){
                        $categ_361 = self::compare_category_class($pid, $isP31);
                         if(!empty($categ_361)){
                             // if categ found
                          //  echo " ## id :".$pid.", cat : ".$categ_361."<br/>";
                            self::set_categ_nongeo($nongeo, $categ_361);
                            $isfound=true;
                            break;
                        }else{
                            //if none is categ then search n+1
                            $w_id = 'Q'.$pid;
                          //  echo " ** no categ found for id ".$pid.", calling self with wid : ".$w_id."<br/>";
                            self::rec_search_wikidata_categ($wikidata, $w_id, $depth, $nongeo, $isP31);
                        }
                    }
                }
                
            }// end if wikiid is not = categ
            
        }// end if depth
     } //end fct
    
    
    /**
    * categorise nongeo items using wikidata classes.
    **/
    public static function categorisation(Request $request){
        $nongeo_wiki = array();
        $meta_tags = array();
        
        $wikidata = new Wikidata();

        $nongeo_wiki = self::find_all_wikidata();
        echo "Nb total of tags nongeo : ".count($nongeo_wiki)." #### <br/>";
      
        $nongeo_0_key = array_search(0, array_column($nongeo_wiki, 'id'));
        unset($nongeo_wiki[$nongeo_0_key]);
        
        $indexStart = $request->indexStart;
        $nb =  $request->nb;
        
        
        $extract = array_slice($nongeo_wiki,$indexStart,$nb);
        
        echo "nb of nongeo to treat :".sizeof($extract);
        
        
        foreach ($extract as $k=>$nongeo){
            
            if($nongeo->id !== 0){
                
                echo "search categ of :".$nongeo->label."<br/>";
                
                //keep only number not the # part of wikidata_id in the url
                $wiki_id = strstr($nongeo->id_wikidata_label, '#', true);
                $position = strpos($nongeo->id_wikidata_label, '#');

                // if no # in id_wikidata use data as is
                if($position === false){
                     $wiki_id = $nongeo->id_wikidata_label;
                }

                self::rec_search_wikidata_categ($wikidata, $wiki_id, 6, $nongeo, false);
            }    
        } // end foreach nongeo
    
        // find meta categ
        $meta_tags = TagController::find_tag_noFK_no_lang();
        foreach($meta_tags as $k=>$tg){
            // check if nongeo item 0 exist if not create it 
            $nongeo_0 = self::find_by_id(0);
            // set nongeo item 0 categ to "meta" 
            if (!isset($nongeo_0)){
          //      echo "** Create nongeo 0 <br/>";
                self::save_NonGeo_inDB(0, "meta", "meta", "meta", 0);
                $nongeo_0 = self::find_by_id(0);
                self::set_categ_nongeo($nongeo_0, "meta");
            }
            //associate tag with nongeo 0
            TagController::update_tag_fk($tg, $nongeo_0, 'nongeo');
          //  echo "associate tag with nongeo 0 <br/>";

    //    echo "********************************************************<br/>";   

        }  //end foreach metatags
        
          
        return View('categorisation_over', ['nb' => $nb, 'indexStart' => $indexStart]);
    } //end fct
    
    
  
}
