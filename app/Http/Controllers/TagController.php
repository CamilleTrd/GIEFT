<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tag;
use App\PhotoTag;
use App\Geonames;
use App\Flickr;
use App\BabelNet;
use App\SemLib;


use AlchemyAPI;

class TagController extends Controller
{
    
     /**
     * Get the tag by its content.
     *
     * @return $tag 
     */
    public static function find_by_label($label) {
        $tag = DB::table('Tag')->where('label', '=' ,$label)->get();
        return $tag;
    }
    
    
    /**
     * Get the tag by its id.
     *
     * @return $tag 
     */
    public static function find_by_id($id) {
        $tag = DB::table('Tag')->where('id', $id)->first();
        return $tag;
    }
     
    
     /**
     * Get the list of tags with no lang and no foreign keys value.
     *
     * @return $tag_list 
     */
    public static function find_tag_noFK_no_lang() {
        $tag_list = DB::table('Tag')->whereNull('nonGeo_id')->whereNull('geo_id')->whereNotIn('lang', ['eng', 'fra', 'duo'])->get();
        return $tag_list;
    }
    
    
    /**
     * Get the tag by its id.
     *
     * @return $tag 
     */
    public static function find_photos_with_tag_id($tag_id, $woeid) {
        $photos = DB::table('Photo')->join('PhotoTag', 'PhotoTag.photo_id', '=', 'Photo.id')->select('Photo.id')->where('PhotoTag.tag_id', $tag_id)->where('Photo.woeid', $woeid)->get();
        return $photos;
    }
    
    
    /**
     * Get the Photo-tag with the given ids.
     *
     * @return $photo_tag 
     */
    public static function find_by_phototag($photo_id, $tag_id) {
        $pt = DB::table('PhotoTag')->where('photo_id', $photo_id)->where('tag_id', $tag_id)->first();
        return $pt;
    }
    
    
    /**
     * Get the tag list with the given photo_id using PhotoTag table.
     *
     * @return $tag_ids 
     */
    public static function find_tags_by_photoid($photo_id) {
        $tag_ids_obj = DB::table('PhotoTag')->select('tag_id')->where('photo_id', $photo_id)->get();
        return $tag_ids_obj;
    }
    
    
    /** 
    * sort an array of tags id according to their categ
    * return an array sorted of tags
    **/
     public static function sort_tags_by_categ($tags) {
         $sorted_tags=array();
         
         foreach($tags as $k=>$t){
             $tag = self::find_by_id($t->tag_id);
             $nongeo = NonGeoController::find_by_id($tag->nonGeo_id);
             
             if($tag->isGeo){
                 // if tag si geo 
                 $sorted_tags['geo feature'][]=$tag;
                 
             }else if($nongeo !== null){
                 // if tag is nongeo
                if($nongeo->category == "weather"){
                    $sorted_tags['weather'][]=$tag;
                }else if($nongeo->category == "temporal"){
                    $sorted_tags['temporal'][]=$tag;
                }else if($nongeo->category == "event"){
                    $sorted_tags['event'][]=$tag;
                }else if($nongeo->category == "color"){
                    $sorted_tags['color'][]=$tag;
                }else if($nongeo->category == "actor"){
                    $sorted_tags['actor'][]=$tag;
                }else if($nongeo->category == "photo"){
                    $sorted_tags['photo'][]=$tag;
                }else if($nongeo->category == "transport"){
                    $sorted_tags['transport'][]=$tag;
                }else if($nongeo->category == "meta"){
                    $sorted_tags['meta'][]=$tag;
                }else if($nongeo->category == "geo class" ){
                    $sorted_tags['geo class'][]=$tag;
                }else if($nongeo->category == "geo feature" ){
                    $sorted_tags['geo feature'][]=$tag;
                }else{
                    //all tags with no categ
                    $sorted_tags['rest'][]=$tag;
                }/*else if($nongeo->category == "geo feature"){
                    $sorted_tags['geo_feature'][]=$t;
                }*/
             }// end if nongeo
         }//end foreach
                 
         return $sorted_tags;
     }

   
     /**
     * Get the PhotoTags instances with the given tag_id and photo woeid.
     * edit in_bbox value with the hierarchy level. ("locality", "county", ...)
     * if "hierarchy" --> tag is a hierarchy element
     */
    public static function edit_photoTag_bbox($woeid, $hierarchy_key, $tag) {        
        $photoTags_obj = DB::table('PhotoTag')->select('photo_id', 'tag_id', 'in_BoundingBox')
                    ->join('Photo', 'PhotoTag.photo_id', '=', 'Photo.id')
                    ->join('Tag', 'PhotoTag.tag_id', '=', 'Tag.id')
                    ->where('Tag.id', $tag->id)
                    ->where('Photo.woeid', $woeid)
                    ->update(['PhotoTag.in_BoundingBox' => $hierarchy_key]);
    }
    
    
    /**
     * Get the PhotoTags instances with the given tag_id and photo_id.
     * edit nongeo weight value with the sem distance. 
     */
    public static function edit_photoTag_nongeo_weight($photo_id, $weight, $tag) {        
        $photoTags_obj = DB::table('PhotoTag')->select('photo_id', 'tag_id', 'nongeo_weight')
                    ->join('Photo', 'PhotoTag.photo_id', '=', 'Photo.id')
                    ->join('Tag', 'PhotoTag.tag_id', '=', 'Tag.id')
                    ->where('Tag.id', $tag->id)
                    ->where('Photo.id', $photo_id)
                    ->update(['PhotoTag.nongeo_weight' => $weight]);
    }
    
    
    /**
     * Get the tags form a given woeid.
     *
     * @return $tags array 
     */
    public static function find_by_woeid($woeid) {
       //retrieve photo_ids from woeid
        $photo_ids = PhotoController::get_photo_ids($woeid);
        
        //for each phot retrieve related tag_id
        $tags= array();
        foreach ($photo_ids as $pid){
            $t_ids_obj=self::find_tags_by_photoid($pid->id);
            foreach($t_ids_obj as $t_obj){
                array_push($tags, $t_obj->tag_id);
            }
        }
        
        return $tags;
    }
    
    
    /**
    * Find if column is empty
    *  @return $col_value
    */
    public static function find_column_value($id, $col_name) {
        $col_value = DB::table('Tag')
            ->where('id', $id)
            ->pluck($col_name);
       
        return $col_value;
    }
    
     /**
     * Update tag FK.
     * 
     */
    public static function update_tag_fk($tag, $fk, $fk_type) {
       if($fk_type !== "geo"){
            DB::table('Tag')
            ->where('id', $tag->id)
            ->update(['nonGeo_id' => $fk->id]);
       }else{
            DB::table('Tag')
            ->where('id', $tag->id)
            ->update(['geo_id' => $fk->id,
                       'isGeo' => true]);
       }
       
    }
    
    
    /**
     * Update tag language.
     */
    public static function update_tag_lang($tag, $lang) {
            DB::table('Tag')
            ->where('id', $tag->id)
            ->update(['lang' => $lang]);
    }
    
    
    /**
     * Update tag is_geo.
     */
    public static function update_tag_is_geo($tag, $is_geo_value) {
            DB::table('Tag')
            ->where('id', $tag->id)
            ->update(['isGeo' => $is_geo_value]);
    }
    
    
     /**
     * Create one tag in DB
     * @return $tag
     */
    public static function create_tag_inDB($tag_label){
        $tag = new Tag;
        //autoincrement id
        $tag->label = $tag_label;
        $tag->save();   
    
        return $tag;
    }
    
    
    /**
     * Save each tag in DB if not already saved.
     */
    public static function save_tags_inDB(array $tags, $photo_id){    

        //save each tag in DB   
        foreach ($tags as $t){
           
           // if tag not already in DB, store tag 
            //[raw] is tag as entered by user ; [_content] is tag w/out spaces and uppercases...
            $tags_found = array();
            $tags_found = self::find_by_label($t['raw']);
            
            //no tags with searched label in DB
            if (empty($tags_found)){
                $tag = self::create_tag_inDB($t['raw']);
                $tag_id=$tag->id;
            }else{
               //echo "tag already in db. ";
                $tag_id=$tags_found['0']->id;
            } 
            
           // create Photo-tag enrtry if not exist.
            $pt_found= self::find_by_phototag($photo_id, $tag_id);
            
            if ($pt_found === null){
                $photo_tag = new PhotoTag;
                $photo_tag->photo_id = $photo_id;
                $photo_tag->tag_id = $tag_id;
                $photo_tag->save(); 
            }    
            
        }//end foreach
    }
    
    
    /**
    * associate tag with geo instance.
    * check for need to duplicate tag instance.
    * $fk_type is "geo or "non-geo"
    *
    * return tag to associate with picture
    **/
    public static function associate_tag_fk($tag, $entity_id, $fk_type){
        // retrieve geo entity.
        if($fk_type=="geo"){
             $entity = GeoController::find_by_id($entity_id);
        }else{
             $entity = NonGeoController::find_by_id($entity_id);
        }
       
        $tag_return = null;
    
        //is tagged already assigned with a geo or non geo element?
       // $isgeo_value = self::find_column_value($tag->id, 'isGeo'); 

        if($fk_type=="geo") {
             $fk_value = self::find_column_value($tag->id, 'geo_id'); 
        }else if($fk_type=="non-geo"){
            $fk_value = self::find_column_value($tag->id, 'nonGeo_id'); 
        }
        
        //if FK value is not empty and is different from $entity_id
        if(!empty($fk_value) && $fk_value !== $entity_id){
            // search is there another tag saved in DB with same label?
            $tags_found = array();
            $tags_found = self::find_by_label($tag->label);
            
            foreach($tags_found as $tf){
                if($fk_type=="geo") {
                     $fk_tf_value = self::find_column_value($tf->id, 'geo_id'); 
                }else if($fk_type=="non-geo"){
                    $fk_tf_value = self::find_column_value($tf->id, 'nonGeo_id'); 
                }

                //if tag with searched entityID as fk value is found
                if ($fk_tf_value == $entity_id){
                    //self::update_tag_fk($tf, $geo, $fk_type);
                    $tag_return = $tf;
                    break;
                }
                
            }   //end foreach
            if($tag_return==null){
                 //duplicate tag and associate geo 
                $new_tag = self::create_tag_inDB($tag->label);
                self::update_tag_fk($new_tag, $entity, $fk_type);  
                $tag_return = $new_tag;
            }
                
        }//end if !empty
        else if (empty($fk_value)){
            //if fk is empty
            self::update_tag_fk($tag, $entity, $fk_type);  
            $tag_return = $tag;
            
        }else{
         //if id is found
            $tag_return = $tag;
        }   
        
        return $tag_return;
    }
    
    
    /**
    * call geonames api search
    **/
    public static function search_geonames(Request $request){
        
        $woeid = $request->woeid;
        $geonames = new Geonames();
        $service = "geonames";
        $tags_ided = array();
        
        $hierarchy_info_ids  = HierarchyController::find_by_woeid($woeid);
        // if hierarchy not already saved find it; else extract form DB
        if ($hierarchy_info_ids === null){
            $hierarchy_info_ids = HierarchyController::get_hierarchy_woeid($request->woeid, 
                                                                          $request->locality, 
                                                                          $request->county, 
                                                                          $request->region, 
                                                                          $request->country, 
                                                                          $geonames);
        }
        
        //store hierarchical entities with alt names
        $hierarchy_info = array(); 
        foreach($hierarchy_info_ids as $key => $geoid){
            $geoObj = $geonames->callMethod("getJSON",['geonameId'=>$geoid]);
            $hierarchy_info[$key] = $geoObj;
            
            // save hierarchy in DB
            $geo_id = $geoObj['geonameId'];
            $geo_name = $geoObj['name'];
            GeoController::save_Geo_inDB($geo_id, $service, false, $geo_name);
        }
           
       // retrieve tags from photos with this woeid 
        $tags_id = array();
        $tags_id = self::find_by_woeid($woeid);
        $tags_id = array_unique($tags_id);
        
        
        foreach($tags_id as $k=>$tid){
            $t=self::find_by_id($tid);
            $tags_array[]=$t;
        }
        
        echo"searching hierarchy match <br/>";
    
        // for each tag label do geonames search call
        foreach ($tags_array as $k=>$t){
            $label = $t->label;
            
            // only try if t is not yet ided.
            if($t->geo_id == null){ 
          
                //compare to hierarchical entities
                $isHierarchy = false;
                foreach($hierarchy_info as $h_key => $hierachy){
                    // for name of entity
                    if(strcasecmp($label, $hierachy['name']) == 0){  

                        $isHierarchy = true;
                        // associate tag with geoname hierarchy élement
                        $tag_r = self::associate_tag_fk($t, $hierachy['geonameId'], "geo");
                        //save hierarchy level in bbox attribute
                        self::edit_photoTag_bbox($woeid, "hierarchy - ". $h_key, $tag_r);    

                        $tags_ided[]=$t;
                        //delete tag from array
                        unset($tags_array[$k]);

                        break;
                    }

                    // for each alternate name of hierarchy entity 
                    foreach($hierachy['alternateNames'] as $h){

                        // hierarchical entity matches save in DB
                        if(strcasecmp($label, $h['name']) == 0){  
                            $isHierarchy = true;
                            // associate tag with geoname hierarchy élement
                            $tag_r = self::associate_tag_fk($t, $hierachy['geonameId'], "geo");
                             //save hierarchy level in bbox attribute
                            self::edit_photoTag_bbox($woeid, "hierarchy - ".$h_key, $tag_r);

                            $tags_ided[]=$t;
                            //delete tag from array
                            unset($tags_array[$k]);

                            break;
                        }
                    }
                } //end foreach hierarchy comparison

            }// end if t already ided geo.
        } //end foreach tag
        
        //save tags left in DB.
        foreach($tags_array as $k=>$t){
            SearchGeonamesController::save($woeid, $t->id, $t->label);
        }
        
        
        return View('search_geonames_over', ['woeid' => $woeid, 'rest_tags' => $tags_array, 'tags_ided' =>$tags_ided, 'hierarchy_level'=> ""]);
    } //end function 
    
    
    
    /**
    * call geonames api search
    **/
    public static function search_geonames_hierarchy(Request $request){
        $woeid = $request->woeid;
        $hierarchy_level = $request->hierarchy_level;
        $geonames = new Geonames();
        $service = "geonames";
        $tags_ided = array();
        $tags_array = array();
      
        //retreive tagIDs from DB ans get tag item 
        //get tags_id
        $tags_id = SearchGeonamesController::find_by_woeid($woeid);
        
        echo "<br/>NB of tags to identify : ".sizeof($tags_id)."<br/>";
        
       // print_r($tags_id);
        
        foreach($tags_id as $k=>$tid){
            $t=self::find_by_id($tid->tag_id);
            $tags_array[]=$t;
        }
        
        
        //if there is tags to identify
        if(!empty($tags_id)){
        
            $hierarchy_info_ids  = HierarchyController::find_by_woeid($woeid);
            // if hierarchy not already saved find it; else extract form DB
            if ($hierarchy_info_ids === null){
                $hierarchy_info_ids = HierarchyController::get_hierarchy_woeid($request->woeid, 
                                                                              $request->locality, 
                                                                              $request->county, 
                                                                              $request->region, 
                                                                              $request->country, 
                                                                              $geonames);
            }

            //store hierarchical entities with alt names
            $hierarchy_info = array(); 
            foreach($hierarchy_info_ids as $key => $geoid){
                $geoObj = $geonames->callMethod("getJSON",['geonameId'=>$geoid]);
                $hierarchy_info[$key] = $geoObj;
            }



            if($hierarchy_level == "world"){
                 echo "serching world<br/>";
                 foreach($tags_array as $k=>$t){

                        // search in ALL world geonames
                        $params_geonames = array(
                            'isNameRequired' => 'true',
                            'q' => $t->label
                             );

                        //call geonames
                        $geoname_results = $geonames->callMethod("search",$params_geonames);  

                        if($geoname_results['totalResultsCount']>0){
                                //if there is results save in db 
                                foreach($geoname_results['geonames'] as $geoname_rslt){
                                     $geo_id = $geoname_rslt['geonameId'];
                                     $geo_name = $geoname_rslt['name'];
                                    // if exact match
                                    if(strcasecmp($t->label, $geo_name) == 0){
                                        // save geoname, associate with tag
                                        GeoController::save_Geo_inDB($geo_id, $service, false, $geo_name);
                                        $tag_r = self::associate_tag_fk($t, $geo_id, "geo");
                                       //save world level in bbox attribute
                                        self::edit_photoTag_bbox($woeid, "world", $tag_r);                           

                                        $tags_ided[]=$t;
                                        //delete from DB
                                        SearchGeonamesController::destroy($t->id);
                                        //delete tag from array
                                        unset($tags_array[$k]);
                                        

                                        break;

                                    }
                                }
                            } //end if parse result 
                 }//end foreach tag

            }//end if world

             else{
               // echo "searching for hierarchy : ".$hierarchy_level."<br/>";
                $hierachy= $hierarchy_info[$hierarchy_level];

                if(!empty($tags_array)){
                    foreach($tags_array as $k=>$t){

                        $params_geonames = array(
                            'north' => $hierachy['bbox']['north'],
                            'south' => $hierachy['bbox']['south'],
                            'west' => $hierachy['bbox']['west'],
                            'east' => $hierachy['bbox']['east'],
                            'isNameRequired' => 'true',
                            'q' => $t->label
                         );

                        //call geonames
                        $geoname_r = $geonames->callMethod("search",$params_geonames);  

                        if(array_key_exists('totalResultsCount', $geoname_r)){
                            $count_result = $geoname_r['totalResultsCount'];
                        }else{
                            $count_result = 0;
                        }

                        if($count_result>0){
                            //if there is results save in db 
                            foreach($geoname_r['geonames'] as $geoname_rslt){
                                 $geo_id = $geoname_rslt['geonameId'];
                                 $geo_name = $geoname_rslt['name'];
                                // if exact match
                                if(strcasecmp($t->label, $geo_name) == 0){
                                    // save geoname, associate with tag
                                    GeoController::save_Geo_inDB($geo_id, $service, false, $geo_name);
                                    $tag_r = self::associate_tag_fk($t, $geo_id, "geo");
                                    //save hierarchy level in bbox attribute
                                    self::edit_photoTag_bbox($woeid, $hierarchy_level, $tag_r);                           

                                    $tags_ided[]=$t;
                                    //delete from DB
                                    SearchGeonamesController::destroy($t->id);
                                    //delete tag from array
                                    unset($tags_array[$k]);

                                    break;

                                }
                            }
                        } //end if parse result 

                    }//end foreach tag
                }//end if !empty tags

            }//end else
        }// if tags not empty
        
        
        return View('search_geonames_over', ['woeid' => $woeid, 'rest_tags' => $tags_array, 'tags_ided' =>$tags_ided, 'hierarchy_level'=>$hierarchy_level]);
    }
    
    
    /**
    * BAbelnet non-geo indentifiaction call
    * 
    **/
    public static function tag_identification_babelnet($babelnet, array $tags_array, $tags_label, $language){
         $lemma = "";
          /*  echo $tags_label;
            echo "<br/><br/>call Babelfy ".$language. " <br/>";
        //  print_r($tags_array);
            echo " <br/>";*/

            //call babelfy !! $language in uppercase iso code
            $response = $babelnet->disambiguate($tags_label, $language);

            if($response !== null){
                // retrieving edges data
                foreach($response as $result) {
                    // retrieving BabelSynset ID
                    $synsetId = $result['babelSynsetID'];
                    $char_start = $result['charFragment']['start'];
                    $char_end = $result['charFragment']['end'];

                   if (array_key_exists($char_start, $tags_array) && ($char_start + $tags_array[$char_start]['tag_length'] -1) == $char_end ){
                        //if charfragment start & end match tag then associate.
                       $respInfo = $babelnet->getSynset($synsetId);
                       $tag_match = self::find_by_id($tags_array[$char_start]['tag_id']);
                        $categ = "";
                       $wikidata_id = "";

                      /* echo "tag match :".$tag_match->label." --> ";
                       print_r($respInfo['senses']['0']['lemma']);
                       echo "<br/>"; */

                       if(!empty($respInfo['categories'])){
                           foreach($respInfo['categories'] as $k=>$cat){
                               if($cat['language'] == 'EN'){
                                   $categ .= ", ".$cat['category'];
                               }
                           }
                       }

                       if(!empty($respInfo['senses'])){
                           $lemma = $respInfo['senses']['0']['lemma'];
                           // search and save wikidata id
                           foreach($respInfo['senses'] as $ky => $sens){
                               if($sens['source'] == "WIKIDATA"){
                                   $wikidata_id = $sens['sensekey'];
                               }
                           }
                        }else{
                           // for exple if synset not identified in the query language, no lemma or definition. But synset exist.
                            $lemma = "unknown_lemma";
                        }

                       // save found synset in DB
                        NonGeoController::save_NonGeo_inDB($synsetId, "Babelnet", $lemma, $categ, $wikidata_id);
                       // associate tag
                        $tag_associated = self::associate_tag_fk($tag_match, $synsetId, "non-geo"); 
                   }

              }// end foreach babelnet response
        }else{
                echo "NULL answer from Babel. <br/>";
            }
    }
    
    /**
    * Search tag lang in Dict Table
    * input label and source ("wiktionary" or "wordnet")
    * return eng or fra or duo
    **/
    public static function find_tag_lang($tag, $source){
        $language= "";
        $dict_array = DictionnaryController::find_by_label_source($tag->label, "wiktionary");
        $nb_dict = count($dict_array);

        if($nb_dict>0){
            $language = $dict_array['0']->lang;   
        } 
        if($nb_dict>1){
             foreach ($dict_array as $k=>$dict){
                 $lang_test = $dict->lang; 
                 if ($language !== $lang_test){
                     $language = "duo";    
                     break;
                 }
             }
        }
        
        //update DB tag->lang
        self::update_tag_lang($tag, $language);
        
        return $language;
    }
    
    
    /**
    * call babelnet api search
    **/
    public static function search_babelnet(Request $request){
        
        $woeid = $request->woeid; 
        $indexStart = $request->indexStart;
        $nbPhotos =  $request->nbPhotos;
        
      //  echo "indexStart : ".$indexStart.", nbPhotos : ".$nbPhotos."<br/>";
        
        $babelnet = new Babelnet();
        
        $photoIDs_processed = array();
        
        //retrieve photos from DB with given WOEID
        $photo_ids = PhotoController::find_by_woeid($woeid);
       // paginate every $nbPhotos photos
        $photo_ids_partial = array_slice($photo_ids, $indexStart, $nbPhotos); 
        
      //   print_r($photo_ids_partial);
        
        foreach ($photo_ids_partial as $p_id){
            
            $photoIDs_processed[] = $p_id->id; 
           // echo "Sarting process of photo ".$p_id->id." --- ";
            // retrieve tag list.
            $tag_list = self::find_tags_by_photoid($p_id->id);
            //echo $tag_list ---> [0] => stdClass Object ( [tag_id] => 11 )
        
            //if photo has tags
            if(!empty($tag_list)){
                //create text with all tags of same picture 
                $tags_label_fr="";
                $tags_label_en="";
                $tags_array_fr =array();
                $tags_array_en =array();
                $index_fr = 0;
                $index_en = 0;
                
                $photo = PhotoController::find_by_id($p_id->id);
                
                foreach ($tag_list as $tag_id){
                    //retrieve tag object.
                    $tag = self::find_by_id($tag_id->tag_id);
                    $tag_length = mb_strlen($tag->label);
                    $language= "";
                    
                    
                    // find language for each tag in local ditionnary table
                    $language = self::find_tag_lang($tag, "wiktionary");
                    
                    // if french tag 
                    if ($language == "fra"){

                        $index_fr = mb_strlen($tags_label_fr);
                        $tags_label_fr .= $tag->label." ";

                        $tags_array_fr[$index_fr] = array(
                                    'tag_id' => $tag_id->tag_id,
                                    'tag_label' => $tag->label,
                                    'tag_length' => $tag_length, 
                                );

                            
                        // if english language    
                        } else if($language == "eng"){
                            
                            $index_en = mb_strlen($tags_label_en);
                            $tags_label_en .= $tag->label." ";
                           
                            $tags_array_en[$index_en] = array(
                                        'tag_id' => $tag_id->tag_id,
                                        'tag_label' => $tag->label,
                                        'tag_length' => $tag_length, 
                                    );
                        
                            //else add to both fr and english
                        } else {
                            $index_fr = mb_strlen($tags_label_fr);
                            $tags_label_fr .= $tag->label." ";

                            $tags_array_fr[$index_fr] = array(
                                        'tag_id' => $tag_id->tag_id,
                                        'tag_label' => $tag->label,
                                        'tag_length' => $tag_length, 
                                    );
                            
                            $index_en = mb_strlen($tags_label_en);
                            $tags_label_en .= $tag->label." ";

                            $tags_array_en[$index_en] = array(
                                        'tag_id' => $tag_id->tag_id,
                                        'tag_label' => $tag->label,
                                        'tag_length' => $tag_length, 
                                    );
                        
                        }//end else
                
                } //end foreach tag_list 
                
               /* print_r($tags_array_fr);
                echo"<br/>";
                print_r($tags_array_en);
                echo"<br/>"; */
                
                $full_photoinfo_en = $tags_label_en ." ".$photo->titre ." ". $photo->description;
                $full_photoinfo_fr = $tags_label_fr ." ".$photo->titre ." ". $photo->description;
                
                
              //  echo "Photo infos EN; ".$full_photoinfo_en."<br/>";
                //echo "Photo infos FR; ".$full_photoinfo_fr."<br/> ************** <br/>";
                
                
                //call babelnet with french and english set
                if (!empty($tags_array_fr)){
                    self::tag_identification_babelnet($babelnet, $tags_array_fr, $full_photoinfo_fr, "FR");
                }
                if (!empty($tags_array_en)){
                    self::tag_identification_babelnet($babelnet, $tags_array_en, $full_photoinfo_en, "EN");
                }
            }
          
       } // end foreach photo_id     
    
        $nb_photos = count($photoIDs_processed); 
        $nb_photos_total = count($photo_ids);
        $nextIndex = $indexStart + $nb_photos;
        
        return View('search_babelnet_over', ['nb_photos_processed' => $nb_photos, 'photo_processed' => $photoIDs_processed, 'woeid' => $woeid, 'nb_photos_total' => $nb_photos_total, 'indexStart' => $indexStart, 'nextIndex' => $nextIndex]);
    
    }
    
    /**
    * Compute semantic distance between 1 noun and an array of noun.
    * keep the best to save
    *
    **/
    public static function semantic_distance($tag_array, $main_tag, $lang, $semLib_instance, $photo_id){
       $results = array();
        
        foreach($tag_array as $t){
            // must be all lower case and space is _ 
            //method GET.            
            // if error : not in dictionnary   
            $main_Tlabel = strtolower($main_tag->label);
            $tlabel = strtolower($t->label);
            $distance_reslt = $semLib_instance->callMethod("nouns",['n1'=>$main_Tlabel, 'n2'=>$tlabel]);
        
            echo "Photo : ".$photo_id." -> Main tag : ".$main_Tlabel." & tag : ".$tlabel." === distance : ".$distance_reslt['distance']."<br/>";
            
            $results[$t->id] = $distance_reslt['distance'];
            
        }
            
        $value_max = max($results);
        $key = array_search($value_max, $results);
        $tag = self::find_by_id($key);

        // echo "<br/> Edit photo tag for tag: ".$main_tag->label." and photo : ".$photo_id;
         self::edit_photoTag_nongeo_weight($photo_id, $value_max, $main_tag);
    }
        
    
    /**
    * retrieve all ambiguous tag with 2 weights and disamb them
    **/
    public static function compare_amb_tag_weights($woeid, $ambTags){
        
        foreach($ambTags as $k=>$tag){
          
            //get all phototag for the given tag
            $photoTags_list = DB::table('PhotoTag')
                ->select('photo_id', 'tag_id', 'nongeo_weight', 'in_BoundingBox')
                ->join('Photo', 'PhotoTag.photo_id', '=', 'Photo.id')
                ->where('Photo.woeid', $woeid)
                ->where('PhotoTag.tag_id', $tag->id)
                ->get();
            
         /*   echo "<br/><br/>";
            print_r($photoTags_list); */
            
            $max_pt_nongWeight = 0;
            $isBBx_country_world = false;
            $isBBx_locality_county_region = false;
            
            
            foreach($photoTags_list as $pt){
                // if geo is world or country prefer nongeo
                
                //search max value of nongeo weight for tag in WOEID   
                 if($pt->nongeo_weight > $max_pt_nongWeight){
                        $max_pt_nongWeight = $pt->nongeo_weight;
                    }      
                
                if(($pt->in_BoundingBox == "country" || $pt->in_BoundingBox == "world")){
                    $isBBx_country_world=true;                   
                }else if(($pt->in_BoundingBox == "locality" || $pt->in_BoundingBox == "region" || $pt->in_BoundingBox == "county")){
                    $isBBx_locality_county_region=true;
                    
                }
                
                //if is hierarchy set isGeo to true.
                else if(startsWith($pt->in_BoundingBox, "hierarchy - ")){
                    // tag is geo - label for hierarchy element
                    $tg = self::find_by_id($pt->tag_id);
                    self::update_tag_is_geo($tg, true);
                    
                    //echo "<br/> SWITCH TAG ".$tg->label." to IsGeo , cause BB = hierarchy <br/>";
                    break;
                }
            }// end foreach $pt
            
            if ($isBBx_country_world){
                 $tg = self::find_by_id($pt->tag_id);
                if($max_pt_nongWeight >= 0.15){
                    //update is_geo of tag to false because nongeo is picked    
                    self::update_tag_is_geo($tg, false);
                   // echo "<br/> SWITCH TAG ".$tg->label." to NOT IsGeo , cause is country or World and weight max = ".$max_pt_nongWeight." <br/>";
                }else{
                    //update is_geo of tag to true because nongeo is picked    
                    self::update_tag_is_geo($tg, true);
                   // echo "<br/> SWITCH TAG ".$tg->label." to IsGeo , cause is country or World and weight max = ".$max_pt_nongWeight." <br/>";
                }
                
            }else if ($isBBx_locality_county_region){
                 $tg = self::find_by_id($pt->tag_id);
                if($max_pt_nongWeight < 0.8){
                    //update is_geo of tag to true because geo is picked    
                    self::update_tag_is_geo($tg, true);
                    //echo "<br/> SWITCH TAG ".$tg->label." to IsGeo , cause is locailty, county or region and weight max = ".$max_pt_nongWeight." <br/>";
                }else{
                    //update is_geo of tag to true because nongeo is picked    
                    self::update_tag_is_geo($tg, false);
                   // echo "<br/> SWITCH TAG ".$tg->label." to NOT IsGeo , cause is locailty, county or region and weight max = ".$max_pt_nongWeight." <br/>";
                }
                
            } // end if isBBX
            
        } // end foreach ambtags   
            
    }
    
    
    /**
    * disambiguate tag meaning when tag have both geo and non geo meaning.
    **/
    public static function geo_nongeo_disambiguation (Request $request){
        //display tags with both FK for given WOEID
        $babelnet = new Babelnet();
        $geonames = new Geonames();
        $semlib = new SemLib();
        
        $woeid = $request->woeid; 
        $amb_tags = array();
        $amb_tags_photos = array();

        $tags_list = self::find_by_woeid($woeid); 
        $tags_list = array_unique($tags_list);

        foreach ($tags_list as $k=>$tid){
            $tag = self::find_by_id($tid);
            if ($tag->geo_id !== null && $tag->nonGeo_id !== null){
                $amb_tags[] = $tag;
            }
        }
        
        //find non geo weight.
        
        // if babelID = geonames ID  (sensekey)
       // filter ambiguity when geo and nongeo identique
        foreach ($amb_tags as $k=>$tg){
            if ($tg->nonGeo_id !== "0" ){
               // set as not found a match.
                $found = false;
                //retrieve sense
                $rsp_obj = $babelnet->getSynset($tg->nonGeo_id);

                foreach($rsp_obj['senses'] as $key=>$result) {
                    $source = $result['source'];
                    $sensekey = $result['sensekey'];
                    $contains_of_geo = array();

                    if ($source == "GEONM" && !$found){
                       if($sensekey == $tg->geo_id){
                            // same identification geoId et NonGeoID.
                           // check is_geo value and exit foreach loop
                         //  echo " # identique <br/>";
                           $found = true;
                           self::update_tag_is_geo($tg, true);
                            unset($amb_tags[$k]);
                           break;
                       }else{

                           // not same geo but still a geo.
                           // verify differences between 2 geonames. 
                           //IS nongeo contained in geo?
                            $contains_of_geo = $geonames->callMethod("contains",['geonameId'=>$tg->geo_id]);

                           if(array_key_exists('value', $contains_of_geo)){
                              if($contains_of_geo['value'] = 13){
                                  $contains_of_geo['geonames'] = '';
                              }
                           }

                            // if we have results check if other geoID is contained.
                           if(!empty($contains_of_geo['geonames'])){
                                foreach($contains_of_geo['geonames'] as $ky => $geoObj){
                                    if($geoObj['geonameId'] == $sensekey){
                                        $found = true;
                                        self::update_tag_is_geo($tg, true);
                                        unset($amb_tags[$k]);
                                        break;
                                    }
                                }
                            }

                            if (!$found){

                               // not same geo but still a geo.
                               // verify differences between 2 geonames. 
                               //IS geo contained in nongeo?
                                $contains_of_nongeo = $geonames->callMethod("contains",['geonameId'=>$sensekey]);    

                                // if we have results check if other geoID is contained.
                               if(!empty($contains_of_nongeo['geonames'])){
                                    foreach($contains_of_nongeo['geonames'] as $ky => $ngeoObj){
                                        if($ngeoObj['geonameId'] == $tg->geo_id){
                                            $found = true;
                                            self::update_tag_is_geo($tg, true);
                                            unset($amb_tags[$k]);
                                            break;
                                        }
                                    }
                                }
                            }// end if !found
                       } 

                    }// end if GN
                } // end foreach resp obj    
            }// if nongeo est != 0 
        }// end foreach ambtags
       
        //$ambtags contains only nongeo now. it has been "cleaned"
        
        // group amb tags with each photo tag list by lang.
        foreach ($amb_tags as $key=>$tag){
            // get all photos with this tag
            $photoIDs_list = self::find_photos_with_tag_id($tag->id, $woeid);            
            
           // print_r($photoIDs_list);
            // echo " --- photoLIST for tag ".$tag->id." and woeid ".$woeid."<br/><br/>";
            foreach($photoIDs_list as $p_id){
                $tag_list = self::find_tags_by_photoid($p_id->id);
             
                foreach($tag_list as $t){
                    $tg= self::find_by_id($t->tag_id);
                    if($tg->lang !== null){
                        $lang = $tg->lang;
                    }else{
                        $lang = self::find_tag_lang($tg, "wiktionary");
                    }
                    
                    
                    if($t->tag_id !== $tag->id){
                        if ($lang=="fra"){
                            $amb_tags_photos[$tag->id][$p_id->id]['FR'][] = $tg;
                        }else if($lang=="duo") {
                            $amb_tags_photos[$tag->id][$p_id->id]['FR'][] = $tg;
                            $amb_tags_photos[$tag->id][$p_id->id]['EN'][] = $tg;
                        }else{
                            $amb_tags_photos[$tag->id][$p_id->id]['EN'][] = $tg;
                        }
                    }else if(sizeof($tag_list)==1 && ($t->tag_id == $tag->id)){
                       // If only one tag in photo -> nongeo weight will be = 1.
                        //discriminate geo found.
                        
                         if ($lang=="fra"){
                            $amb_tags_photos[$tag->id][$p_id->id]['FR'][] = $tg;
                        }else if($lang=="duo") {
                            $amb_tags_photos[$tag->id][$p_id->id]['FR'][] = $tg;
                            $amb_tags_photos[$tag->id][$p_id->id]['EN'][] = $tg;
                        }else{
                            $amb_tags_photos[$tag->id][$p_id->id]['EN'][] = $tg;
                        }
                    }
                } // end foreach tag of photo
                
                
            } // end foreach photo   
        }// end foreach ambigous tag
        
      
        //call semlib
        foreach ($amb_tags as $k=>$tga){
           //call semlib and calc distance sem
            if($tga->lang !== null){
                $lang_mainTag = $tga->lang;
            }else{
                $lang_mainTag = self::find_tag_lang($tg, "wiktionary");
            } 
         
          //  echo "<br/> TGA >id : ".$tga->id.", lang : ".$lang_mainTag; 
            // foreach photo compute sem dist with same language tag only. do only EN for now.
            if($lang_mainTag == "eng" || $lang_mainTag == "duo"){
                foreach($amb_tags_photos[$tga->id] as $p_id=>$photo_list_tag){
                    self::semantic_distance($photo_list_tag['EN'], $tga, 'EN', $semlib, $p_id);
                }
                
            }else{
                // for now skip french version 
             /*    foreach($amb_tags_photos[$tag->id] as $main_id=>$list_tag){
                    self::semantic_distance($amb_tags_photos[$tag->id][$p_id->id]['FR'], $tag, 'FR', $semlib, "nouns");
                }*/
            }
         }//end call semlib
        
        
        self::compare_amb_tag_weights($woeid, $amb_tags);
    
        return View('disambiguation_over', ['tags_disamb' => $amb_tags,  'woeid' => $woeid]);
        
    } // end function
    
    
    
}
