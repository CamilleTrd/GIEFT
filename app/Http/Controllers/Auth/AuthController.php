<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Session;
use Redirect;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use View;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }
    
      
    protected function showLoginForm(){
        return View::make('auth/login');
    }
    
     protected function showRegistrationForm(){
       // Log::debug("in show reg. form !!!");
        return View::make('auth/register');
    }
    
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:4',
            //'password' => 'required|confirmed|min:4',
        ]);
    }
    
    public static function validator_login(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
		    'password' => 'required|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public static function create(array $user){
      //  echo "validatior not fail";
        return User::create([
            'name' => $user['name'],
            'email' => $user['email'],
            'password' => bcrypt($user['password']),
        ]);
        return Redirect::intended('home')->send();
    }
    
  
    public static function login($data){
        
      $userdata = array(
            'name' => $data['name'],
            'password' =>$data['password']
          );
      // doing login.
      if (Auth::validate($userdata)) {
        if (Auth::attempt($userdata)) {
         //   echo "** login ok, now redirect to home. **<br/>";
            echo "Si vous n'êtes pas redirigé automatiquement : <a href='/home'>cliquer ici</a>";
            return View::make('home');
        }
      } 
      else {
        // if any error send back with message.
        Session::flash('error', 'Something went wrong'); 
        return Redirect::to('login')->send();
      }

    }
    
    public static function logout(){
         //Log::debug("in logout authcont. !!!");
        Auth::logout(); // logout user
        Session::flush();
          echo "** logout ok, now redirect to home. **<br/>";
            echo "Si vous n'êtes pas redirigé automatiquement : <a href='/home'>cliquer ici</a>";
        return Redirect::to('login')->send(); //redirect back to login
    }
    
}
