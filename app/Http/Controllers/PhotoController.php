<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Flickr;
use App\Photo;

class PhotoController extends Controller
{
    /**
     * Get the photo with the given ID.
     *
     * @return $photo 
     */
    public static function find_by_id($id) {
        $photo = DB::table('Photo')->where('id', $id)->first();
        return $photo;
    }
    
    /**
     * Get the photo id list with a given WOEID.
     *
     * @return $photo 
     */
    public static function find_by_woeid($woeid) {
        $photoID_list = DB::table('Photo')->select('id')->where('woeid', $woeid)->get();
        return $photoID_list;
    }
    
    
    /**
     * Get the photo list with a given userID.
     *
     * @return $photo_list 
     */
    public static function find_photos_by_user($user) {
        $photo_list = DB::table('Photo')->where('url', 'LIKE', 'https://www.flickr.com/photos/'.$user.'/%')->get();
        return $photo_list;
    }
    
    
    /**
     * Get the photo id list with a given title.
     *
     * @return $photo_list 
     */
    public static function find_by_titre($titre) {
        $photo_list = DB::table('Photo')->where('titre', $titre)->get();
        return $photo_list;
    }
    
      /**
     * Get the WOEID of a given photoID.
     *
     * @return $woeid 
     */
    public static function find_woeid_by_id($pid) {
        $woeid = DB::table('Photo')->select('woeid')->where('id', $pid)->first();
        return $woeid;
    }
    
    
    /**
     * Get list of woeids in DB.
     *
     * @return $woeids 
     */
    public static function get_woeids() {
        $woeids = DB::table('Photo')->select('woeid')->distinct()->get();
        return $woeids;
    }
    
    
    /**
     * Get list of photos for a specific woeid.
     *
     * @return $photo_list 
     */
     public static function get_photo_list($woeid) {
         if($woeid == 'all'){
              $photo_list = DB::table('Photo')->get();
         }else{
              $photo_list = DB::table('Photo')->where('woeid', $woeid)->get();
         }
         return $photo_list;
    }

    
    /**
     * Get list of ids in DB dor a specific woeid.
     *
     * @return $ids 
     */
     public static function get_photo_ids($woeid) {
         $ids = DB::table('Photo')->select('id')->where('woeid', $woeid)->get();
         return $ids;
    }
    
    
    /**
    * count number of tag for photo given photo ID
    * return integer
    **/
    public static function count_tag($photoID) {
         $nb_tag = DB::table('PhotoTag')->select('tag_id')->where('photo_id', $photoID)->count();
         return $nb_tag;
    }
    
    
    /**
     * Save each photo Id in DB if not already saved.
     */
    public function save_photos_inDB(array $photos, $woeid){    

        //save each photo in DB   
        foreach ($photos as $p){

           // if photo_id not already in DB, store photo
            $photo_id = self::find_by_id($p['id']);
            
          //  echo "id " . $p['id'];
            if ($photo_id === null){
                $photo = new Photo;
                $photo->id = $p['id'];
                $photo->woeid = $woeid;

                $photo->save(); 
            } 
        }
    }
    
    
     /**
     * Get result page to save in DB.
     *
     * @return View
     */
    public function save_result_page(Request $request){
        $woeid = $request->woeid;
        $page = $request->page;
        $flickr = new Flickr();

        // search flickr call api.
        $method = 'flickr.photos.search';
        $params = array(
            'woe_id' => $woeid,
            'per_page' => '500',
            'page' => $page
        );

        $search = $flickr->callMethod($method, $params);
        $photos = $search['photos']['photo'];

        //add each photo id & woeid to DB
        self::save_photos_inDB($photos, $woeid);

        //display message saving ok.
        return View('search_results_saved', 
                    ['page' => $page,
                     'pages' => $search['photos']['pages'],
                     'woeid' => $woeid]);
    }
    
    
     /**
     * Get photos infos for each foto with given woeid and save in DB the values.
     *
     * @return View
     */
    public function get_photo_infos(Request $request){
        $woeid = $request->woeid;
        // retrieve all ids
        $photo_ids = self::get_photo_ids($woeid);
        
        // Call flickr api to get infos foreach photos
        $flickr = new Flickr();
        $method = 'flickr.photos.getInfo';
        
       foreach ($photo_ids as $id){
           //check if photoinfo already saved if not get info from FLickr
           $p = self::find_by_id($id->id);
           if($p->url === null){
               // get photo info
                $photo_info = $flickr->callMethod($method, ['photo_id' => $id->id]);

               //if photo fails
                if ($photo_info['stat'] != 'fail'){
                      // save in DB 
                    self::save_photo_info_inDB($photo_info); 
                }
           }//end if empty url
           
        }//end foreach
    
        return View('photos_infos_saved', ['woeid' => $woeid]);
    }
    
    
    /**
     * Save photo info in DB. if contains tag else delete photo
     */
     public function save_photo_info_inDB(array $photo_info){
         // find photo instance with id
         $photoInfo = $photo_info['photo'];
         
         if (!empty($photoInfo['tags']['tag'])){  
             // fill up columns in Photo Table
             DB::table('Photo')
                ->where('id', $photoInfo['id'])
                ->update([ 'lat' => $photoInfo['location']['latitude'],
                           'long' => $photoInfo['location']['longitude'],
                           'url' => $photoInfo['urls']['url']['0']['_content'],
                           'titre' => $photoInfo['title']['_content'],
                           'description' => $photoInfo['description']['_content']]);
             
             
             //Create Tags and Photo-Tags instances here 
             TagController::save_tags_inDB($photoInfo['tags']['tag'], $photoInfo['id']);
            

         }else{
             //delete photo from DB
              DB::table('Photo')
                ->where('id', $photoInfo['id'])
                ->delete();
         }
        
     }
    
     
    /**
    * Browse view of picture info
    **/
    public function browse_photo(Request $request){
        $woeid = $request->woeid;
        $random = $request->random;
        $indexStart = $request->index_start;
        $nb_photos = $request->nb_photos;
        $nb_photos_total = $request->nb_photos_total;

        $nb_tags = array();

        $list_photos = self::get_photo_list($woeid);
       // echo count($list_photos)."<br/>";

        if($random){
            $rand_list = array_rand($list_photos, 20);
            foreach($rand_list as $key=>$index){
                $return_list[] = $list_photos[$index];
            }
        }else{
             // paginate every $nbPhotos photos
            $return_list = array_slice($list_photos, $indexStart, $nb_photos); 
        }

        foreach($return_list as $k=>$photo){
            //getnumber of tag for photo.
            $nb_tags[$photo->id] = PhotoController::count_tag($photo->id);
        }

         return View('browse_results',
                         ['random' => $random, 
                          'photo_list' =>  $return_list, 
                          'nb_tags' => $nb_tags,
                          'nb_photos' => count($return_list),
                          'nb_photos_total' => count($list_photos),
                          'index_start' => $indexStart,
                          'woeid' => $woeid]);
    }

    
     /**
    * Browse view of picture info after REST test
    **/
    public function browse_photo_result(Request $request){
        $woeid = $request->woeid;
        $random = $request->random;
        $indexStart = $request->index_start;
        $nb_photos = $request->nb_photos;
        $nb_photos_total = $request->nb_photos_total;
    
        $nb_tags = array();

        $list_photos = RESTvalidationController::get_photo_list($woeid);
       // echo count($list_photos)."<br/>";
       // print_r($list_photos);

        if($random){
            $rand_list = array_rand($list_photos, 20);
            foreach($rand_list as $key=>$index){
                $return_list[] = $list_photos[$index];
            }
        }else{
             // paginate every $nbPhotos photos
            $return_list = array_slice($list_photos, $indexStart, $nb_photos); 
        }

         return View('browse_rest_results',
                         ['random' => $random, 
                          'photoID_list' =>  $return_list, 
                          'nb_photos' => count($return_list),
                          'nb_photos_total' => count($list_photos),
                          'index_start' => $indexStart,
                          'woeid' => $woeid]);
    }
    
    
     /**
    * clean DB of photo duplicates
    **/
    public function cleanDB_duplicates(){
         $p_toDelete=array();
       
        // first case : picture same title (but not empty title) same nb of tags
       // SELECT Photo.titre, COUNT(*) FROM Photo WHERE titre <> "" GROUP BY Photo.titre HAVING COUNT(*) > 1
         $photo_duplicates = DB::table('Photo')
                ->select(DB::raw('Photo.titre, COUNT(*)'))
                ->where('titre', "<>", "")
                ->groupBy('Photo.titre')
                ->having('COUNT(*)', '>', 1)
                ->get();
        
        //get photo id of each duplictes and keep only one.
        foreach($photo_duplicates as $p_dup){
            //retreive photos items
            $photo_list=self::find_by_titre($p_dup->titre);
            foreach($photo_list as $k=>$p){
                if($k>0){
                    $p_toDelete[]=$p->id;
                }
            }
        }//end foreach $photo_duplicates
        
        echo "Duplicate (title) have been listed: <br/>";
         foreach($photos_toDelete as $p){
            echo $p.", ";
        }
         echo "<br/>";
        //efectively delete pictures
        DB::table('Photo')->whereIn('id', $p_toDelete)->delete();
        echo "Duplicate (title) have been deleted.<br/>";
        
      return View('clean_db_over');
        
    }
    
    
    /**
    * clean DB of photo similars (same author, same tags)
    **/
    public function cleanDB_similar(Request $request){
        if(!empty($request->photos_toDelete)){
            $ptd=$request->photos_toDelete;
            $photos_toDelete = explode(",", $ptd);
        }else{
            $photos_toDelete=array();
        }
        $index=$request->idx;
        $index_main = "";
        $to_delete=array();
        
        // second clean duplicates with != titles but similar tags and url
               
        //retreive all photo for given woeid
        $photo_list = self::get_photo_list("all");
        $current_user = "";
        $nb_user= 0;

        $photo_list_extract = array_slice($photo_list,$index);
        $count = sizeof($photo_list);

        echo "1st photo ID of array : ". $photo_list_extract[0]->id.", cut at index : ".$index."<br/>";
        
        
          //   $plist_user = self::find_photos_by_user("helveto");
        
        foreach($photo_list_extract as $idx=>$photo){
            $main_photo_tagsID = array();
            
            //if current photo has been selected to be deleted dont process
            echo "Main photo : ".$photo->id."<br/>";
            $k = array_search($photo->id,$photos_toDelete);
           // echo "is photo in delete array : ".$k." -- ";
            if($k === false){
                 //retreive url and get all phots from list with same owner
                    $pieces = explode("/", $photo->url);
                    $user= $pieces['4'];

                    echo "Curr_user : ".$current_user." & user : ".$user."<br/>";
                    if($current_user !== $user){
                        //if user is different 
                        $nb_user += 1;
                        $current_user = $user;
                    }
                      echo "increment user value : ".$nb_user."<br/>";
                
                    if($nb_user < 20){
                            //get all photos form same user 
                        $photo_list_user = self::find_photos_by_user($user);
                        //retreive current photo id and remove from user photo list 
                        $k = array_search($photo->id, array_column($photo_list_user, 'id'));
                        unset($photo_list_user[$k]);

                        $plist_user_to_delete = $photo_list_user;

                        $nbTags_main = self::count_tag($photo->id);
                        $main_tagsID = TagController::find_tags_by_photoid($photo->id);
                        foreach($main_tagsID as $t){
                            $main_photo_tagsID[] = $t->tag_id;
                        }

                     //   echo "Size plist usr : ".sizeof($photo_list_user)."<br/>";
                        echo " nb tag : ".$nbTags_main."<br/>";

                       /* echo "<pre>";
                        print_r($plist_user_to_delete);
                        echo "</pre>";*/

                        foreach($photo_list_user as $p){
                            $photo_tagsID= array();
                            echo "<br/>Photo id ".$p->id.", "; 
                            //get nb of tags 
                            $nbTags = self::count_tag($p->id);
                            /*if($nbTags !== $nbTags_main){
                                echo "not same NB tags ".$nbTags."-- keep photo (unset) <br/>";

                                //if not same number of tags remove from list of user photos
                                //add one to index because array satrt at index 1 (first ubset line 365)
                                 $key = array_search($p->id, array_column($photo_list_user, 'id'))+1;
                                 unset($plist_user_to_delete[$key]);
                            }else{*/
                            if($nbTags == $nbTags_main){
                                echo "same NB tags -- ";
                                //same nb of tags, test if same list of tags.
                                $diff_reslt1=array();
                                $diff_reslt2=array();
                                //test if array is different
                                $p_tagsID = TagController::find_tags_by_photoid($p->id);
                                foreach($p_tagsID as $tid){
                                    $photo_tagsID[] = $tid->tag_id;
                                }

                                $diff_reslt1 = array_diff($main_photo_tagsID, $photo_tagsID);
                                $diff_reslt2 = array_diff($photo_tagsID, $main_photo_tagsID);
                                if(sizeof($diff_reslt1) == 0 && sizeof($diff_reslt2) == 0){
                                     echo "same tags -- delete photo<br/>";
                                     $to_delete[]=$p->id;

                                }else{
                                    echo "not same tags -- keep photo <br/>";
                                    echo"main_photo_tagsID : ";
                                    print_r($main_photo_tagsID);
                                    echo"<br/>photo_tagsID : ";
                                    print_r($photo_tagsID);
                                    echo"<br/>";
                                    //not same array --> dont delete photo, unset photo of array
                                   /* $key = array_search($p->id, array_column($photo_list_user, 'id'))+1;
                                    unset($plist_user_to_delete[$key]);*/
                                }
                            }//end if mm nb tags
                            else{
                                echo "not same NB tags -- keep photo<br/>";
                            }

                        /*    echo "photos to delete : <pre>";
                            print_r($to_delete);
                            echo "</pre>";*/


                        }//End foreach photolist user

                        $photos_toDelete = array_merge($photos_toDelete, $to_delete);
                        //remove duplicates
                        $photos_toDelete = array_unique($photos_toDelete);
                        

                    }//end if nbuser >5
                    else{
                        
                         $index_main = array_search($photo->id, array_column($photo_list, 'id'));
                         $to_delete = implode(",",$photos_toDelete);
                        
                         echo "<br/><br/>".sizeof($photos_toDelete)." duplicates (tags list) have been listed. until photoid : ".$photo->id.", at main_key : ".$index_main." <br/>";
                        //efectively delete pictures
                      //  echo $to_delete. "<br/>";
                      //  DB::table('Photo')->whereIn('id', $photos_toDelete)->delete();
                      //  echo "Duplicate (tags list) have been deleted. <br/>";
                        
                       
                        
                        return View('clean_db_over_similar', ['idx'=>$index_main, 'photos_toDelete'=>$to_delete, 'count'=>$count, 'over'=>false]);
                        
                    }


                } // end if k
            }// end foreach $photo_list_extract
        
         $to_delete = implode(",",$photos_toDelete);
        
         echo "<br/><br/>".sizeof($photos_toDelete)." duplicates(tags list) have been listed. No more Pictures to process deleting following pictures : <br/>";
        //efectively delete pictures
        echo $to_delete."<br/>";
        DB::table('Photo')->whereIn('id', $photos_toDelete)->delete();
        echo "Duplicate (tags list) have been deleted.<br/>";

        return View('clean_db_over_similar', ['idx'=>null, 'photo_toDelete'=>$to_delete, 'count'=>$count, 'over'=>true]);
        
    }
    
}