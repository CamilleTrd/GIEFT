<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Geonames;
use App\Hierarchy;


class HierarchyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    
    public function index()
    {
        //
    } */

   
    /**
     * Get the hierarchy record with the given ID.
     *
     * @return $hierarchy 
     */
    public static function find_by_woeid($woeid) {
        $hierarchy = DB::table('Hierarchy')
            ->select('locality', 'county', 'region', 'country')
            ->where('woeid', $woeid)
            ->first();
        
        return $hierarchy;
    }
    
    
    
    /**
     * Store a newly created resource in DB.
     */
    public static function save_hierarchy_inDB($woeid, $hierarchy_info)
    {
           // if $woeid not already in DB, store hierarchy element
            $hierarchy = self::find_by_woeid($woeid);
        
            if ($hierarchy === null){
                $hierarchy = new Hierarchy;
                $hierarchy->woeid = $woeid;
                foreach($hierarchy_info as $h_key=>$h){
                   if($h !== "not_found"){
                        $hierarchy->$h_key = $h;
                   }
                }
                $hierarchy->save(); 
            } 
    }
    
    
    /**
    * retrieve hierarchy from woeid
    **/
    public static function get_hierarchy_woeid($woeid, $locality, $county, $region, $country, Geonames $geonames){
        $hierarchy = array(
            
            'locality' => $locality,
            'county' => $county,
            'region' => $region,
            'country' => $country
        );

        //retieve hiercarchical entities in geonames
        $hierarchy_info_id = array(); 
        foreach($hierarchy as  $h => $h_value){
            $params_geonames=array();

            switch ($h) {
                case "locality":
                    /* $params_geonames = array(
                        'featureClass'  => 'P',
                        'featureCode'   => 'PPL',
                        'isNameRequired' => 'true',
                        'q' => $h_value
                    );
                    
                    OR */
                    $params_geonames = array(
                        'featureClass'  => 'A',
                        'featureCode'   => 'ADM3',
                        'isNameRequired' => 'true',
                        'q' => $h_value
                    );
                    
                break;
                case "county":
                   $params_geonames = array(
                        'featureClass'  => 'A',
                        'featureCode'   => 'ADM2',
                        'isNameRequired' => 'true',
                        'q' => $h_value
                    );
                break;
                case "region":
                    $params_geonames = array(
                        'featureClass'  => 'A',
                        'featureCode'   => 'ADM1',
                        'isNameRequired' => 'true',
                        'q' => $h_value
                    );
                 break;
                case "country":    
                    $params_geonames = array(
                        'featureClass'  => 'A',
                        'featureCode'   => 'PCLI',
                        'isNameRequired' => 'true',
                        'q' => $h_value
                    );  
                 break;
            }

            $rslt= $geonames->callMethod("search", $params_geonames);

            //store geonames id returned from seach call
            if($rslt['totalResultsCount']>0){
                $hierarchy_info_id[$h] = $rslt['geonames']['0']['geonameId'];
            }else{
                $hierarchy_info_id[$h] = "not_found";
            }            
        }
        
        //fill Hierarchy table with found geonames
        self::save_hierarchy_inDB($woeid, $hierarchy_info_id);
        
        return $hierarchy_info_id;
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
 
    public function show($id)
    {
        //
    } */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function edit($id)
    {
        //
    }  */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function update(Request $request, $id)
    {
        //
    } */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
   
    public function destroy($id)
    {
        //
    } */
}
