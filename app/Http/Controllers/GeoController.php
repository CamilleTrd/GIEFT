<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Tag;
use App\Geo;
use App\Geonames;

class GeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    
    public function index()
    {
        //
    } */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    
    public function create()
    {
        //
    } */

    /**
     * Get the geo with the given ID.
     *
     * @return $geo 
     */
    public static function find_by_id($id) {
        $geo = DB::table('Geo')->where('id', $id)->first();
        return $geo;
    }
    
    
    
    /**
     * Store a newly created resource in DB.
     */
    public static function save_Geo_inDB($geoID, $serviceName, $isClassCode, $geoName)
    {
           // if $geo_id not already in DB, store geo element
            $geo_id = self::find_by_id($geoID);
        
            if ($geo_id === null){
                $geo = new Geo;
                $geo->id = $geoID;
                $geo->is_class_code = $isClassCode;
                $geo->service_name = $serviceName;
                $geo->geo_label =$geoName;
                $geo->save(); 
            } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
 
    public function show($id)
    {
        //
    } */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function edit($id)
    {
        //
    }  */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function update(Request $request, $id)
    {
        //
    } */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
   
    public function destroy($id)
    {
        //
    } */
}
