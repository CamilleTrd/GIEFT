<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Geonames;
use App\SearchGeonames;


class SearchGeonamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    
    public function index()
    {
        //
    } */

   
    /**
     * Get the $tag_list record with the given WOEID.
     *
     * @return $tag_list 
     */
    public static function find_by_woeid($woeid) {
        $tag_list = DB::table('SearchGeonames')
            ->select('tag_id')
            ->where('woeid', $woeid)
            ->get();
        
        return $tag_list;
    }
    
    
     /**
     * Get the id record with the given WOEID and TAG ID.
     *
     * @return $searchID 
     */
    public static function find_id($tagID, $woeid) {
        $searchID = DB::table('SearchGeonames')
            ->select('id')
            ->where('woeid', $woeid)
            ->where('$tagID', $tagID)
            ->first();
        
        return $searchID;
    }
    
     /**
     * Store a geoname search value.
     */
    public static function save($woeid, $tagID, $label)
    {
        $geoSearch = new SearchGeonames;
        //autoincrement id
        $geoSearch->woeid = $woeid;
        $geoSearch->tag_id = $tagID;
        $geoSearch->tag_label = $label;
        $geoSearch->save(); 
            
    }
    
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
 
    public function show($id)
    {
        //
    } */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function edit($id)
    {
        //
    }  */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    
    public function update(Request $request, $id)
    {
        //
    } */

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
   */
     public static function destroy($tag_id)
    {
         $searchID = DB::table('SearchGeonames')
            ->where('tag_id', $tag_id)
            ->delete();
        
    } 
    
     /**
     * Remove the all resource with given woeid from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
   */
     public static function destroy_by_woeid($woeid)
    {
         $searchID = DB::table('SearchGeonames')
            ->where('woeid', $woeid)
            ->delete();
        
    } 
    
    
    
}
