<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoTag extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PhotoTag';
   
    /**
     * Indicates if the model should be timestamped.
     * NO created_at and updated_at attributes
     * @var bool
     */
    public $timestamps = false;
    
    public function photo()
    {
        return $this->belongsTo('Photo');
    }
     
    public function tag()
    {
        return $this->belongsTo('Tag');
    }
    
    public function rest_validation()
    {
        return $this->hasMany('RESTvalidation');
    }
}
