<?php
namespace App;

class Geonames {

    private $username = '';
    //hourly limit of 2000 credits for citylocate >> http://www.geonames.org/export/ws-overview.html
    
    private $urlRest = "http://api.geonames.org/";

    private $format = "json";
    
    
    public function __construct() {
        
    }

    public function getUsername(){
        return $this->username;
    }
    

    public function callMethod($method, array $params) {
        
        /*$params['q']  =  $text;
        $params['south']  =  $south;
        $params['north']  =  $north;
        $params['west']  =  $west;
        $params['east']  =  $east;
        $params['isNameRequired'] = $nameRequired;*/
        $params['username'] = $this->username; 
        $params['type'] = $this->format;
      
        $encoded_params = array();
       
        foreach ($params as $k => $v){
            $encoded_params[] = urlencode($k).'='.urlencode($v);
        }

        $urlMethode =  $this->urlRest."".$method."?";
        $url = $urlMethode.implode('&', $encoded_params);
        
    //    echo "Call geonames : ".$url."<br/>";
        
        $rsp = file_get_contents($url);
        $rsp_obj = json_decode($rsp, true);
        return $rsp_obj;
    }
}