<?php
namespace App;

class SemLib {

    private $urlRest = "http://localhost:12000/";

   
    public function __construct() {
       
    }
   

    public function callMethod($method, array $params) {
        
       
       /*if uses nouns
        if($method == "nouns"){
            $params['n1'] = noun1;
            $params['n2'] = noun2;
        }else{
            //if uses indexes
            $params['i1'] = index1;
            $params['i2'] = index2;
        }*/
      
        $encoded_params = array();
       
        foreach ($params as $k => $v){
            $encoded_params[] = urlencode($k).'='.urlencode($v);
        }

        $urlMethode =  $this->urlRest."".$method."?";
        $url = $urlMethode.implode('&', $encoded_params);
        $rsp = @file_get_contents($url);
        
        $rsp_obj = array();
    
        if($rsp===FALSE) {
            $rsp_obj['distance'] = 0;
        } else { 
            $rsp_obj = json_decode($rsp, true);
        }
       
        
        return $rsp_obj;
        
    
    }
}