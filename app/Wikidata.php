<?php
namespace App;

class Wikidata {
   
    private $urlRest = "https://www.wikidata.org/wiki/Special:EntityData/";
    private $format = "json";
    
    
    public function __construct() {
    }


    public function callMethod($qid) {
    
        $url =  $this->urlRest."".$qid.".".$this->format;
        
        $rsp = file_get_contents($url);
        $rsp_obj = json_decode($rsp, true);
        return $rsp_obj;
    }
}