<?php
namespace App;

class Flickr {

    private $apiKey = '';

    private $urlRest = "https://api.flickr.com/services/rest/?";

    private $format = "json";
    
    
    public function __construct() {
        

    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function callMethod($method, array $params) {
        
        $params['method']  =  $method;
        $params['api_key'] = $this->apiKey; 
        $params['nojsoncallback'] = 1;
        $params['format'] = $this->format;
        $encoded_params = array();
        foreach ($params as $k => $v){
            $encoded_params[] = urlencode($k).'='.urlencode($v);
        }

        $url = $this->urlRest.implode('&', $encoded_params);
        $rsp = file_get_contents($url);
        $rsp_obj = json_decode($rsp, 1);
        return $rsp_obj;
    }
}