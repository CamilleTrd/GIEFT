<?php
namespace App;

class BabelNet {

    private $key_b = '';
    //daily limit of 1000 credits default
    
    private $urlRest = "https://babelnet.io/v2/";
 
    public function __construct() {

    }

    public function getKey(){
        return $this->key_b;
    }
    
    /**
    * Babelfy
    */
    public function disambiguate($text, $lang){
        $urlBbly = 'https://babelfy.io/v1/';
        $methodBbly = 'disambiguate';
        
        
        $params['text'] = $text;
        $params['lang'] = $lang;
        $params['key'] = $this->key_b;
        
         $encoded_params = array();
       
        foreach ($params as $k => $v){
            //treatment for many langs param.
            if ($k == 'langs' && $method == 'getSynsetIds'){
                foreach($v as $lang){
                    $encoded_params[] = urlencode($k).'='.urlencode($lang);
                }
            }else{
                $encoded_params[] = urlencode($k).'='.urlencode($v);
            }
                
            
        }

        $urlMethode =  $urlBbly."".$methodBbly."?";
        $url = $urlMethode.implode('&', $encoded_params);
        
      //  echo $url;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
       
        $rsp_obj = json_decode(curl_exec($ch), true);
        curl_close($ch);
    
       
       // use output 
        /*
         # retrieving edges data
          foreach($response as $result) {

            // retrieving token fragment
            $tokenFragment = $result['tokenFragment'];
            $tfStart = $tokenFragment['start'];
            $tfEnd = $tokenFragment['end'];

            echo "Start token fragment: " . $tfStart
                        . "<br/>" . "End token fragment: " . $tfEnd;

            // retrieving char fragment
            $charFragment = $result['charFragment'];
            $cfStart = $charFragment['start'];
            $cfEnd = $charFragment['end'];

            echo "<br/>Start char fragment: " . $cfStart
                . "<br/>" . "End char fragment: " . $cfEnd;

            // retrieving BabelSynset ID
            $synsetId = $result['babelSynsetID'];
            echo "<br/>BabelNet Synset id: " . $synsetId;
          }
        
        */
        
        return $rsp_obj;
    }
    
    
    
    /**
    * Retrieve the IDs of the Babel synsets (concepts) denoted by a given word
    * call the actual rest call method.
    */
    public function getSynsetIDs($word) {
        $method = 'getSynsetIds';
        
        $lang1 = 'EN';
        $lang2 = 'FR';
        //$lang3 = 'DE';

        $params = [ 'word' => $word,
                    'langs' => [ $lang1, $lang2],
                    'key' => $this->key_b];
        
       $rsp_obj = self::callMethod($method, $params);
        
         return $rsp_obj;
        //output
         /** 
        foreach($rsp_obj as $result) {
            echo $result['id'] . '<br/>';
        }
        **/
    }
    
    
    /**
    *  Retrieve the information of a given synset
    * can pass wordnet id.
    * call the actual rest call method.
    */
    public function getSynset($id) {
        $method = 'getSynset';
       
        /* otionnal : max 3 lang
        $lang1 = 'EN';
        $lang2 = 'FR';
        
        $params['langs'] = $lang1;
        $params['langs'] = $lang2; */
        
        
        $params['id'] = $id;
        $params['key'] = $this->key_b;
        
       $rsp_obj = self::callMethod($method, $params);
        
        return $rsp_obj;
        // output 
        /**
        
        # retrieving BabelSense data
        foreach($rsp_obj['senses'] as $result) {
        $lemma = $result['lemma'];
        $language = $result['language'];
        echo "Language: " . $language
            . "<br/>Lemma: " . $lemma . "<br/><br/>";
        }

        # retrieving BabelGloss data
        foreach($rsp_obj['glosses'] as $result) {
        $gloss = $result['gloss'];
        $language = $result['language'];
        echo "Language: " . $language
            . "<br/>Gloss: " . $gloss . "<br/><br/>";
        }

        # retrieving BabelImage data
        foreach($rsp_obj['images'] as $result) {
        $url = $result['url'];
        $language = $result['language'];
        $name = $result['name'];
        echo "Language: " . $language
            . "<br/>Name: " . $name
            . "<br/>Url: " . $url . "<br/><br/>";
        }
        **/
    }
    
    
    /**
    *  Retrieve the senses of a given word, needs word language!
    * call the actual rest call method.
    */
    public function getSenses($word, $langs) {
        $method = 'getSenses';
        
     //   $params['langs'] = 'EN';
        $params['word'] = $word;
        $params['key'] = $this->key_b;
        
       $rsp_obj = self::callMethod($method, $params);
       
        return $rsp_obj;
        
        // output 
        /**
        foreach($rsp_obj as $result) {
            $lemma = $result['lemma'];
            $language = $result['language'];
            $source = $result['source'];
            echo "Language: " . $language
                . "<br/>Lemma: " . $lemma
                . "<br/>Source: " . $source . "<br/><br/>";
          }
        **/
    }
    
    
    /**
    *  Retrieve edges of a given BabelNet synset
    *
    * call the actual rest call method.
    */
    public function getEdges($synsetId) {
        $method = 'getEdges';
        
        $params['id'] = $synsetId;
        $params['key'] = $this->key_b;
        
        $rsp_obj = self::callMethod($method, $params);
        
        return $rsp_obj;
        // output 
        /**
         # retrieving edges data
          foreach($response as $result) {
            $target = $result['target'];
            $language = $result['language'];

            $pointer = $result['pointer'];
            $relation = $pointer['name'];
            $group = $pointer['relationGroup'];
            echo "Language: " . $language
                . "<br/>Target: " . $target
                . "<br/>Relation: " . $relation
                . "<br/>Relation group: " . $group . "<br/><br/>";
          }
          
          OR for hypernyms, hyponyms and antonyms 
          
          # retrieving edges data
          foreach($response as $result) {
            $target = $result['target'];
            $language = $result['language'];

            $pointer = $result['pointer'];
            $relation = $pointer['name'];
            $group = $pointer['relationGroup'];

            # Types of relationGroup: HYPERNYM,  HYPONYM, MERONYM, HOLONYM, OTHER
            if ((strpos(strtolower($group), "hypernym") !== FALSE)
                || (strpos(strtolower($group), "hyponym") !== FALSE)
                || (strpos(strtolower($relation), "antonym") !== FALSE)) {

                echo "Language: " . $language
                    . "<br/>Target: " . $target
                    . "<br/>Relation: " . $relation
                    . "<br/>Relation group: " . $group . "<br/><br/>";
            }
          }
        **/
    }
    
    
    
    function callMethod($method, $params) {
        
        $encoded_params = array();
       
        foreach ($params as $k => $v){
            //treatment for many langs param.
            if ($k == 'langs' && $method == 'getSynsetIds'){
                foreach($v as $lang){
                    $encoded_params[] = urlencode($k).'='.urlencode($lang);
                }
            }else{
                $encoded_params[] = urlencode($k).'='.urlencode($v);
            }
                
            
        }

        $urlMethode =  $this->urlRest."".$method."?";
        $url = $urlMethode.implode('&', $encoded_params);
        
        $rsp = file_get_contents($url);
        $rsp_obj = json_decode($rsp, true);
        
      //  print_r($rsp_obj);
       
       // use output (see each function for correct extraction) here after return        
        return $rsp_obj;
    }
}


 
